import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	TextInput,
	View,
	AsyncStorage,
	TouchableHighlight,
	Image,
	Dimensions,
	TabBarIOS,
	Navigator,
	TouchableWithoutFeedback,
	TabsRoute,
	StatusBar
} from 'react-native';

// import styles from './public/styles/styles.js';

import Welcome from './app/Components/Welcome.js';

import GetTeams from './app/Components/GetTeams.js';

import ChooseTeams from './app/Components/ChooseTeams.js';

import CreateTeam from './app/Components/CreateTeam.js';

import Layout from './app/Components/Layout.js';

import Team from './app/Components/Team.js';

import db from './app/db/db.js';

import moment from 'moment';

let now = moment().format();

export default class clipboard extends Component {
	constructor(props) {
		super(props)
		this.state = {
			teams: []
		}
	}


	componentWillMount(){

		// debugger; //find teams
		// db.remove({ playerQtID: {$exists: true}}, { multi: true }, function (err, numRemoved) {
		// 		console.log(numRemoved);
		// })
		db.find({playerQtID: {$exists: true}}, function(err,docs){
			console.log(docs);
		})

		db.find({}, function (err, docs) {
			console.log('TIME: ', now);
			console.log('db snapshot: ', docs);

			this.setState({
				teams: docs
			});
		}.bind(this));

	}

	renderScene(route, navigator) {
		// debugger;
		if(route.name == 'Welcome') {
			return <Welcome navigator={navigator} teams={this.state.teams}/>
		}
		if(route.name == 'GetTeams') {
			return <GetTeams navigator={navigator} teams={this.state.teams}/>
		}
		if(route.name == 'CreateTeam') {
			return <CreateTeam navigator={navigator} teams={route.passProps}/>
		}

		if(route.name == 'ChooseTeams') {
			return <ChooseTeams navigator={navigator} teams={route.passProps}/>
		}
		if(route.name == 'Layout') {
			return <Layout navigator={navigator} team={route.passProps} players/>
		}
		if(route.name == 'Team') {
			return <Team navigator={navigator} team={route.passProps} />
		}
	}


	render() {
		// debugger;
		return (
			<Navigator
			initialRoute={{statusBarHidden: true}}
			style={{ flex:1 }}
			initialRoute={{ name: 'Welcome' }}
			renderScene={this.renderScene.bind(this)} />
		);
	}


}

AppRegistry.registerComponent('clipboard', () => clipboard);
