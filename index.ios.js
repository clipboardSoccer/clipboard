import React, { Component } from 'react';
import {
	AppRegistry,
	AsyncStorage,
	Navigator,
	StatusBar
} from 'react-native';

import Welcome from './app/Components/Welcome.js';

import GetTeams from './app/Components/GetTeams.js';

import ChooseTeams from './app/Components/ChooseTeams.js';

import CreateTeam from './app/Components/CreateTeam.js';

import Layout from './app/Components/Layout.js';

import Team from './app/Components/Team.js';

import db from './app/db/db.js';

import moment from 'moment';

let now = moment().format();

export default class clipboard extends Component {
	constructor(props) {
		super(props)
		this.state = {
			teams: []
		}
	}

	// Initiate by grabbing everything that is currently stored in AsyncStorage
	componentWillMount(){
	// setState everything inside the db
		db.find({}, function (err, docs) {
			console.log('snapshot: ', docs);
			this.setState({
				teams: docs
			});
		}.bind(this));
// db.remove({}, { multi: true }, function (err, numRemoved) {
// });

	}
	//Navigator is pushed as props to every page 
	renderScene(route, navigator) {
		// debugger;
		if(route.name == 'Welcome') {
			return <Welcome navigator={navigator} teams={this.state.teams}/>
		}
		if(route.name == 'GetTeams') {
			return <GetTeams navigator={navigator} teams={this.state.teams}/>
		}
		if(route.name == 'CreateTeam') {
			return <CreateTeam navigator={navigator} teams={route.passProps}/>
		}

		if(route.name == 'ChooseTeams') {
			return <ChooseTeams navigator={navigator} teams={route.passProps}/>
		}
		if(route.name == 'Layout') {
			return <Layout navigator={navigator} team={route.passProps} players/>
		}
		if(route.name == 'Team') {
			return <Team navigator={navigator} team={route.passProps} />
		}
	}


	render() {
		return (
			<Navigator
			initialRoute={{statusBarHidden: true}}
			style={{ flex:1 }}
			initialRoute={{ name: 'Welcome' }}
			renderScene={this.renderScene.bind(this)} />
		);
	}


}

AppRegistry.registerComponent('clipboard', () => clipboard);
