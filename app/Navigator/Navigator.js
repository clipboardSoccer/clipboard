import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	TextInput,
	View,
	AsyncStorage,
	TouchableHighlight,
	Image,
	Dimensions,
	TabBarIOS,
	Navigator,
	TouchableWithoutFeedback,
	TabsRoute,
	StatusBar
} from 'react-native';

// import styles from './public/styles/styles.js';

import Welcome from '../Components/Welcome';

import db from './app/db/db.js';

export default class Navigator extends Component {
	constructor(props) {
		super(props)
		this.state = {
			teams: this.props.teams
		}
	}

	renderScene(route, navigator) {
		if(route.name == 'Welcome') {
			return <Welcome navigator={navigator} teams={this.state.teams}/>
		}
	}

	render() {
		debugger;		
		return (
			<Navigator
			style={{ flex:1 }}
			initialRoute={{ name: 'Welcome' }}
			renderScene={ this.renderScene } />        
		);
	}
}