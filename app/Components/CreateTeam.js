import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	TextInput,
	View,
	AsyncStorage,
	TouchableHighlight,
	Image,
	Dimensions,
	TabBarIOS,
	Navigator,
	TouchableWithoutFeedback,
	TabsRoute,
	StatusBar
} from 'react-native';

import CalendarPicker from 'react-native-calendar-picker';

import styles from './public/styles/CreateTeamStyles.js';

import ChooseTeams from './ChooseTeams';

import Layout from './Layout';

import db from '../db/db.js';

import Button from 'react-native-button'

export default class CreateTeam extends Component {
	constructor(props) {
		super(props)
		this.state = {
			teams: this.props.teams,
		}
	}

	 useNewTeam(teams){
	 	this.props.navigator.push({
			name: 'ChooseTeams',
			passProps: teams
		});
	 }

	 goBack(teams){
	 	this.props.navigator.push({
			name: 'ChooseTeams',
			passProps: teams
		});
	 }

	 handleSubmit(){

		let newTeam = {
			Team_Name: this.state.teamName,
		}

		db.insert(newTeam, function (err, cb){
			this.setState({
				teams: [cb]
			});
			this.useNewTeam(this.state.teams);
		}.bind(this));

	 }


	getTeam(){
		return(
			<View style={styles.pageContainer}>
				<Button
				style={styles.back}
				onPress={() => this.goBack(this.state.teams)}
				>
					Back
				</Button>
				<Text style={styles.header}>
					Create A Team
				</Text>
				<TextInput
				style={styles.input}
				placeholder='Team Name'
				maxLength={15}
				clearButtonMode='always'
				onChangeText={(value) => this.setState({teamName: value})}
				value={(this.state.teamName)}
				/>

				<TouchableHighlight
				style={styles.submitContainer}
				onPress={this.handleSubmit.bind(this)}
				>
					<Text style={styles.submitText}>
						Submit
					</Text>

				</TouchableHighlight>
			</View>
		)
	}


	render() {

			return (
				<View>
					{this.getTeam()}
				</View>
			)
	}
}
