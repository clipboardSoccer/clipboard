import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	TextInput,
	View,
	AsyncStorage,
	TouchableHighlight,
	Image,
	Dimensions,
	TabBarIOS,
	NavigatorIOS,
	TouchableWithoutFeedback,
	Navigator
} from 'react-native';

import styles from './public/styles/styles.js';


// import Datastore from 'react-native-local-mongodb';
// const db = new Datastore({ filename: 'asyncStorageKey', autoload: true });

export default class Login extends Component {

  _handleChangePage() {
    navigator.push({
    	title: "Second Page",
    	component: SecondPage,
    	passProps: {
        	toggleNavBar: this.props.toggleNavBar,
     	}
    });

 }

  render() {
  	debugger;
    return (
      <View style={styles.loginContainer}>
        <Text>Welcome To Clipboard!</Text>

        <TouchableWithoutFeedback onPress={this._handleChangePage}>
          <View>
            <Button onPress={this._handleChangePage}>
            	Get Started!
            </Button>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
