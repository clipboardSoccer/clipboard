import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	Image,
	ScrollView,
	StatusBar,
} from 'react-native';
import Button from 'react-native-button';
import styles from './public/styles/WelcomeScreenStyles';
import GetTeams from './GetTeams';

import soccerBall from '../../public/images/soccerBall.png';
import grassTexture from '../../public/images/grassTexture.png';

export default class Welcome extends Component {
	constructor(props) {
		super(props)
		this.state = {
		}
	}

	//Press "Get Started" to navigate => GetTeams page
	componentWillMount(){
		// debugger;
	}

	getStarted(){
		this.props.navigator.push({
		name: 'GetTeams',
		});
	}

	render() {

		return (
			// <ScrollView style={styles.welcomeContainer}>
				<View style={styles.welcomeContainer}> 
					<StatusBar hidden={true} />
					<View style={styles.imageContainer}>
						<Image source={grassTexture}
						style={styles.backgroundImage}
						 />
					</View>
					<View style={styles.welcomeScreen}>
						<Text style={styles.banner}>
							Welcome to Clipboard
						</Text>
						<TouchableHighlight
							underlayColor= {'rgba(255,255,255,0.0)'}
							style={styles.click}
							onPress={this.getStarted.bind(this)}
							>
							<Image source={soccerBall}
							style={styles.soccerBall}
							>
								<Text style={styles.getStarted}>
									Get Started!
								</Text>
							</Image>
						</TouchableHighlight>
					</View>
				</View>
			// </ScrollView>
		);
	}
}
