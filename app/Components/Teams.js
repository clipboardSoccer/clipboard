import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	TextInput,
	View,
	AsyncStorage,
	TouchableHighlight,
	Image,
	Dimensions,
	TabBarIOS,
	NavigatorIOS,
	TouchableWithoutFeedback,
	TabsRoute,
	StatusBar
} from 'react-native';

import Button from 'react-native-button';

import styles from './public/styles/TeamsStyles.js';

import CreateTeam from './CreateTeam';

import db from '../db/db.js';

// import jersey from '../../public/images/jersey.png'

export default class Teams extends Component {
	constructor(props) {
		super(props)
		this.state = {
			teamName: this.props.teamName,
			startDate: this.props.startDate,
			id: this.props.id,
		}
	}

	createTeam(team){

		this.props.navigator.push({
			name: 'CreateTeam',
			passProps: team
		});
	}

	handleSelect(){

		db.find({ teamId: this.props.id }, function (err, doc) {
			this.setState({
				players: doc
			});

			let team = this.state;

			if(doc.length === 0) {
				this.props.navigator.push({
					name: 'Team',
					passProps: team
				});
			} else {
				this.props.navigator.push({
					name: 'Layout',
					passProps: team
				});
			}
		}.bind(this));
	}

	handleRemove(team){
		db.remove({ _id: team.id }, {}, function (err, numRemoved) {
			db.remove({ teamId: team.id }, { multi: true }, function (err, numRemoved) {
				db.find({}, function (err, docs) {
					this.setState({
						teams: docs
					});
					if(docs.length < 1){
						this.createTeam(docs);
					} else {
						this.props.navigator.push({
							name: 'ChooseTeams',
							passProps: docs
						});
					}
				}.bind(this));

			}.bind(this));

		}.bind(this));
	}

	render() {

			return (
			<View style={styles.teamsPageContainer}>
				<View style={styles.teamContainer}>

					<TouchableHighlight
					underlayColor= {'#777'}
					onPress={ () => this.handleSelect() }
					style={styles.selectTeam}
					>
						<Text style={styles.teamName}>
							{this.state.teamName}
						</Text>

					</TouchableHighlight>
				</View>
				<View style={styles.removeContainer}>
					<Button
					onPress={ () => this.handleRemove(this.state) }
					style={styles.selectRemove}
					>
						<Text style={styles.selectRemoveText}>
							X
						</Text>
					</Button>
				</View>
			</View>
			)
	}
}
