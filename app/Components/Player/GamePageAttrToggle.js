import React, { Component } from 'react';
import {
	Text,
	TouchableHighlight,
	ScrollView,
	Platform,
	View,
	Alert
} from'react-native';

import Button from 'react-native-button'
import styles from '../public/styles/GamePageModalStyles'
import db from '../../db/db.js';
import GamePagePlayer from './GamePagePlayer'

const SPEED_MSG = 'Speed Updated!'
const SHOT_MSG = 'Shot Updated!'
const CTRL_MSG = 'Control Updated!'
const PASS_MSG = 'Pass Updated!'

export default class GamePageAttrToggle extends Component {
	constructor(props) {
		super(props)
		this.state = {
			playerSpeed: parseInt(this.props.playerSpeed),
			playerPass: parseInt(this.props.playerPass),
			playerShot: parseInt(this.props.playerShot),
			playerCtrl: parseInt(this.props.playerCtrl)
		}
	}
	componentDidMount(){

	}

	handleToggle(value, updateType){
		//FOR DEBUGGING======================================
		// db.find({}, function (err, docs) {
		// 	console.log(docs);})
		// debugger;
		console.log('this is player: ' + this.props.playerLastName);
		// debugger;
		//===================================================

    //this switch uses the string passed in through updateType and updates the player document and resets the state

  	switch (updateType) {
			case "Speed":
  			if(value){
					db.update(
					{ _id: this.props.playerId },
	  				{ $set:
	            { 'Speed': this.state.playerSpeed + 1 }
	  				},
					function (err, numAffected){
						// console.log(numAffected);
	          // console.log("updated speed");
						// console.log(this.state.playerSpeed);
						this.state.playerSpeed++


						Alert.alert(SPEED_MSG)

					}.bind(this));
				}
				else{
					db.update(
					{ _id: this.props.playerId },
	  				{ $set:
	            { 'Speed': this.state.playerSpeed - 1 }
	  				},
					function (err, numAffected){
						// console.log(numAffected);
						// console.log("updated speed")
						this.state.playerSpeed--;


						Alert.alert(SPEED_MSG)

					}.bind(this));
				}
  			break;

			case "Ctrl":
				if(value){
					db.update(
					{ _id: this.props.playerId },
						{ $set:
							{ 'Control': this.state.playerCtrl + 1 }
						},
					function (err, numAffected){
						// console.log(numAffected);
						// console.log("updated control");
						// console.log(this.state.playerCtrl);

						this.state.playerCtrl++


						Alert.alert(CTRL_MSG)

					}.bind(this));
				}
				else{
					db.update(
					{ _id: this.props.playerId },
						{ $set:
							{ 'Control': this.state.playerCtrl - 1 }
						},
					function (err, numAffected){
						console.log(numAffected);
						// console.log("updated control");
						// console.log(this.state.playerCtrl);

						this.state.playerCtrl--;


						Alert.alert(CTRL_MSG)


					}.bind(this));
				}
				break;

			case "Shot":
				if(value){
					db.update(
					{ _id: this.props.playerId },
						{ $set:
							{ 'Shooting': this.state.playerShot + 1 }
						},
					function (err, numAffected){
						// console.log(numAffected);
						// console.log("updated shot");
						// console.log(this.state.playerShot);

						this.state.playerShot++


						Alert.alert(SHOT_MSG)


					}.bind(this));
				}
				else{
					db.update(
					{ _id: this.props.playerId },
						{ $set:
							{ 'Shooting': this.state.playerShot - 1 }
						},
					function (err, numAffected){
						// console.log(numAffected);
						// console.log("updated shot");
						// console.log(this.state.playerShot);

						this.state.playerShot--;


						Alert.alert(SHOT_MSG)


					}.bind(this));
				}
  			break;

			case "Pass":
				if(value){
					db.update(
					{ _id: this.props.playerId },
						{ $set:
							{ 'Passing': this.state.playerPass + 1 }
						},
					function (err, numAffected){
						// console.log(numAffected);
						// console.log("updated pass");
						// console.log(this.state.playerPass);

						this.state.playerPass++


						Alert.alert(PASS_MSG)

					}.bind(this));
				}
				else{
					db.update(
					{ _id: this.props.playerId },
						{ $set:
							{ 'Passing': this.state.playerPass - 1 }
						},
					function (err, numAffected){
						// console.log(numAffected);
						// console.log("updated pass");
						// console.log(this.state.playerPass);

						this.state.playerPass--;


						Alert.alert(PASS_MSG)

					}.bind(this));
				}
  			break;
  		default: console.log('Error: No value updated.');

  	}

	}
	modalClose(){
		this.props.callModalClose();
	}

	render() {
    let { playerNumber, playerFirstName, playerLastName, playerPos1, playerSpeed, playerShot,playerPass, playerCtrl } = this.props

		if(Platform.OS === 'ios'){
			return(
					<View>
	          <View style={styles.gpModalAttrRow}>
	  					<TouchableHighlight
								style={styles.gpModalAttrButton} onPress={() => this.handleToggle(false, "Speed")}>
								<View>
									<Text style={styles.gpModalAttrToggleText}>-</Text>
								</View>
	  					</TouchableHighlight>

	              <Text style={styles.gpModalAttrText}>Speed</Text>

	  					<TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(true, "Speed")}>
								<View>
									<Text style={styles.gpModalAttrToggleText}>+</Text>
								</View>
	  					</TouchableHighlight>
	          </View>
	          <View style={styles.gpModalAttrRow}>
	            <TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(false, "Shot")}>
	              <View>
	                <Text style={styles.gpModalAttrToggleText}>-</Text>
	              </View>
	            </TouchableHighlight>
	            <View>
	              <Text style={styles.gpModalAttrText} >Shot</Text>
	            </View>
	            <TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(true, "Shot")}>
	              <View>
	                <Text style={styles.gpModalAttrToggleText}>+</Text>
	              </View>
	            </TouchableHighlight>
	          </View>
	          <View style={styles.gpModalAttrRow}>
	            <TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(false, "Ctrl")}>
	              <View>
	                <Text style={styles.gpModalAttrToggleText}>-</Text>
	              </View>
	            </TouchableHighlight>
	            <View>
	              <Text style={styles.gpModalAttrText}>Control</Text>
	            </View>
	            <TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(true, "Ctrl")}>
	              <View>
	                <Text style={styles.gpModalAttrToggleText}>+</Text>
	              </View>
	            </TouchableHighlight>
	          </View>
	          <View style={styles.gpModalAttrRow}>
	            <TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(false, "Pass")}>
	              <View>
	                <Text style={styles.gpModalAttrToggleText}>-</Text>
	              </View>
	            </TouchableHighlight>
	            <View>
	              <Text style={styles.gpModalAttrText}>Passing</Text>
	            </View>
	            <TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(true, "Pass")}>
	              <View>
	                <Text style={styles.gpModalAttrToggleText}>+</Text>
	              </View>
	            </TouchableHighlight>
	          </View>

						<TouchableHighlight  style={styles.gpModalCloseButton} onPress={() => this.modalClose()}>
								<Text style={styles.gpModalCloseText}>Close</Text>
						</TouchableHighlight>
				</View>
			);
		}//end of ios
		else if(Platform.OS === 'android'){
			return(
					<View>
						<View style={styles.gpModalAttrRow}>
							<TouchableHighlight
								style={styles.gpModalAttrButton} onPress={() => this.handleToggle(false, "Speed")}>
								<View>
									<Text style={styles.gpModalAttrToggleText}>-</Text>
								</View>
							</TouchableHighlight>

								<Text style={styles.gpModalAttrText}>Speed</Text>

							<TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(true, "Speed")}>
								<View>
									<Text style={styles.gpModalAttrToggleText}>+</Text>
								</View>
							</TouchableHighlight>
						</View>
						<View style={styles.gpModalAttrRow}>
							<TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(false, "Shot")}>
								<View>
									<Text style={styles.gpModalAttrToggleText}>-</Text>
								</View>
							</TouchableHighlight>
							<View>
								<Text style={styles.gpModalAttrText} >Shot</Text>
							</View>
							<TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(true, "Shot")}>
								<View>
									<Text style={styles.gpModalAttrToggleText}>+</Text>
								</View>
							</TouchableHighlight>
						</View>
						<View style={styles.gpModalAttrRow}>
							<TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(false, "Ctrl")}>
								<View>
									<Text style={styles.gpModalAttrToggleText}>-</Text>
								</View>
							</TouchableHighlight>
							<View>
								<Text style={styles.gpModalAttrText}>Control</Text>
							</View>
							<TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(true, "Ctrl")}>
								<View>
									<Text style={styles.gpModalAttrToggleText}>+</Text>
								</View>
							</TouchableHighlight>
						</View>
						<View style={styles.gpModalAttrRow}>
							<TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(false, "Pass")}>
								<View>
									<Text style={styles.gpModalAttrToggleText}>-</Text>
								</View>
							</TouchableHighlight>
							<View>
								<Text style={styles.gpModalAttrText}>Passing</Text>
							</View>
							<TouchableHighlight style={styles.gpModalAttrButton} onPress={() => this.handleToggle(true, "Pass")}>
								<View>
									<Text style={styles.gpModalAttrToggleText}>+</Text>
								</View>
							</TouchableHighlight>
						</View>

						<TouchableHighlight  style={styles.gpModalCloseButton} onPress={() => this.modalClose()}>
								<Text style={styles.gpModalCloseText}>Close</Text>
						</TouchableHighlight>
						<View style={styles.aBuffer}></View>
				</View>
			);
		}//end of android else if
	}
}
