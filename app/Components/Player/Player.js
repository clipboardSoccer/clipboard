import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
	View
} from 'react-native';

import styles from '../public/styles/PlayerStyles.js';

export default class Player extends Component {
	constructor(props) {
		super(props)
		this.state = {
			playerFirstName: this.props.playerFirstName,
			playerLastName: this.props.playerLastName,
			playerNumber: this.props.playerNumber,
			Control: this.props.Control,
			Passing: this.props.Passing,
			Shooting: this.props.Shooting,
			Size: this.props.Size,
			Speed: this.props.Speed,
			id: this.props.id
		}
	}

	render() {
		return(
			<View>
				<View style={styles.playerViewWrapperContainer}>
					<View style={styles.playerContainer}>

						<Text style={styles.playerName}>
							{this.props.playerFirstName}
						</Text>
						<Text style={styles.playerName}>
							{this.props.playerLastName}
						</Text>

						<Text style={styles.playerNumber}>
							{this.props.playerNumber}
						</Text>

						<Text
						style={styles.edit}
						onPress={() => {this.props.editPlayer(this.state)} } 
						>
							edit
						</Text>
					</View>
					<View style={styles.playerContainer}>

						<Text style={styles.player}>
							Control: {this.props.Control}
						</Text>

						<Text style={styles.player}>
							Passing: {this.props.Passing}
						</Text>
						<Text style={styles.player}>
							Shooting: {this.props.Shooting}
						</Text>

						<Text style={styles.player}>
							Size: {this.props.Size}
						</Text>

						<Text style={styles.player}>
							Speed: {this.props.Speed}
						</Text>
					</View>
				</View>
			</View>
		);
	}
}
