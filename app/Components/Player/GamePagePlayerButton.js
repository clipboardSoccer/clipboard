import React, { Component } from 'react';
import {
	Text,
	TouchableHighlight,
	ScrollView,
	Modal,
	View,
	TouchableWithoutFeedback,
	Animated,
	} from 'react-native';

import Button from 'react-native-button';
import GamePageAttrToggle from './GamePageAttrToggle'
import styles from '../public/styles/GamePageModalStyles.js'


let ACTION_TIMER = 400;
let COLORS = ['rgb(255,255,255)', 'rgb(111,235,62)'];

export default class GamePagePlayerButton extends Component {
	constructor(props) {
		super(props)
		this.state = {
			//MODAL TOGGLES ====================================================
			modalVisible: false,
			animationType: "slide", //or "fade", or "none"
			transparent: true,
			//==================================================================
		}
	}

  _setModalVisible(visible){
		this.setState({modalVisible: visible});
	}
	_modalCloseHelper(cb){
		// debugger
		this._setModalVisible(!this.state.modalVisible)
	}

 	_setAnimationType(type){
		this.setState({animationType: type })
	}

	_toggleTransparent() {
		this.setState({transparent: !this.state.transparent});
	}

	render() {

		// debugger;


		let {playerNumber, playerFirstName, playerLastName, agg, pos1, pos2, pos3, _id, teamId, Speed, Shooting, Passing, Control} = this.props

		return(
			<View>
				<Modal animationType={this.state.animationType}
				transparent={this.state.transparent}
				visible={this.state.modalVisible}
				onRequestClose={() => {alert("Modal Closed!")}}
				>
					<View style={styles.gpModalContainer}>
							<GamePageAttrToggle
							playerId={_id}
							playerTeam={teamId}
							playerNumber={playerNumber}
							playerFirstName={playerFirstName}
							playerLastName={playerLastName}
							playerSpeed={Speed}
							playerCtrl={Control}
							playerPass={Passing}
							playerShot={Shooting}
							playerPos1={pos1}
							callModalClose={this._modalCloseHelper.bind(this)}
							>
							</GamePageAttrToggle>
					</View>
				</Modal>
				<View style={styles.buttonContainer}>
					<Button onPress={() => {this._setModalVisible(true)}}
					style={styles.buttons}
					team={teamId}
					fName={playerFirstName}
					lName={playerLastName}
					pNum={playerNumber}
					agg={agg}
					p1={pos1}
					p2={pos2}
					p3={pos3}
					>
						#{playerNumber}
					</Button>
				</View>
			</View>
		);
	}
}
