import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	TouchableOpacity,
	ScrollView
} from 'react-native';

import moment from 'moment';
import db from '../../db/db.js';
import styles from '../public/styles/gamePageButtonStyles.js';
import timer from 'react-native-timer';

const QUARTER_LENGTH = 5
const START_MESSAGE = "Game Start!"
const QUARTER_MESSAGE = "Next Quarter!"
const END_MESSAGE = "End of Game"
let triggerCheck = false
let quarterCounter = 0

export default class GamePageGameStartButton extends Component {
	constructor(props){
		super(props)

			this.state = {
				team: this.props.team,
				timerMessage: START_MESSAGE,
				// timerStatus: false,
				currentQuarter: quarterCounter,
				endGame: '',
				clock: ''
			}
	}
	_onGameStartPress(){
		//i dont know why this if statement can hold props and the next one cant but it works
		if(triggerCheck === false){
			quarterCounter++;
			this.props.getQuarter(quarterCounter);
		}

		if(triggerCheck === false){

			triggerCheck = true;
			// let startTime = QUARTER_LENGTH * 60;
			// let counter = QUARTER_LENGTH * 60;
			let startTime = QUARTER_LENGTH ;
			let counter = QUARTER_LENGTH ;

			startTime = this._timeConverter(startTime);

			//starts off the timer
			this.setState({
				timerMessage: '',
				clock: startTime,
				currentQuarter: quarterCounter,
				endGame: false
			})

			//marks the last quarter
			if(quarterCounter === 4){
				this.setState({
					endGame: true
				})
			}

			//counts down
			timer.setInterval(this, 'tock' ,() => {
				// console.log(this.state.clock);

				this.state.clock = this._timeConverter(counter--)

				this.setState({clock: this.state.clock})

				if(counter === 0){
					triggerCheck = false;

					if(!this.state.endGame){
						this.setState({
							timerMessage: QUARTER_MESSAGE,
							clock: startTime
						})
					}
					else {
						quarterCounter = 0;
						triggerCheck = true;

						this.setState({
							timerMessage: END_MESSAGE,
							currentQuarter: quarterCounter,
							endGame: false,
							clock: startTime
						});
					}

					this._clearTime('tock');
				}//end of if

			}, 1000 )//end of set interval
		}//end of triggerCheck
	}//end of function

	_timeConverter(t){
		//It takes the current time in seconds and converts it to minutes and seconds (mm:ss).
		let minutes = Math.floor(t/60);
		let seconds = t - (minutes * 60);

		if (seconds < 10){
			seconds = "0" + seconds;
		}

		if (minutes === 0){
			minutes = "00";
		} else if (minutes < 10){
			minutes = "0" + minutes;
		}

		return minutes + ":" + seconds;
}


	_clearTime(name){
		this.props.filterField();
		timer.clearInterval(this, name)
		console.log('time cleared!');
	}

	// _currentQuarter(){
	// 	debugger;
	// 	console.log(this.state.currentQuarter);
	// 	return this.state.currentQuarter
	// }

	componentDidMount(){
	}

	componentWillMount(){
	  // debugger;
	}

	componentWillUnmount(){
		// debugger
		console.log("timer cleared!");
		timer.clearinterval(this._tick);
	}


	render(){
			return (
				<View style={styles.gamePageButtonMainContainer}>
					<View style={styles.startButtonContainer}>
						<TouchableOpacity style={styles.startQuarterButton} onPress={this._onGameStartPress.bind(this)}>
							<Text style={styles.startQuarterText} >{ this.state.timerMessage }</Text>
						</TouchableOpacity>
					</View>
					<View style={styles.quarterInfoContainer}>
						<View style={styles.quarterContainer}>
							<Text style={styles.quarterText}>Quarter: {this.state.currentQuarter }</Text>
						</View>
						<View style={styles.remainingContainer}>
							<Text style={styles.remainingText}>Remaining: { this.state.clock }</Text>
						</View>
					</View>
				</View>
			)
	}
}
