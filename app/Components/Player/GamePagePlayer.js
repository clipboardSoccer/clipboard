import React, { Component } from 'react';
import {
	Text,
	TouchableHighlight,
	ScrollView,
	View
} from 'react-native';

import styles from '../public/styles/gamePageStyles'
import db from '../../db/db.js';

export default class GamePagePlayerAttrToggle extends Component {
	constructor(props) {
		super(props)
		this.state = {
		}
	}
	componentDidMount(){

	}

	render() {

		return(
				<View style={styles.gpModalView}>
					<View>
						<Text>
							#{this.props.playerNumber} {this.props.playerFirstName} {this.props.playerLastName}
						</Text>
						<Text>
							Position: {this.props.playerPos1}
						</Text>
					</View>
				</View>
		);
	}
}
