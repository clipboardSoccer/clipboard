import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	TabBarIOS,
	ScrollView,
	Platform
} from 'react-native';

import ScrollableTabView from 'react-native-scrollable-tab-view';

import styles from './public/styles/styles.js';

import db from '../db/db.js';

import CheckIn from './CheckIn.js';
import GamePage from './Pages/GamePage.js';
import Blank from './Blank.js';


export default class Layout extends Component {
	constructor(props) {
		super(props)
		this.state = {
			selectedTab: 'checkIn',
			team: this.props.team
		}
	}

	_renderContent(renderPage){
		return (
			<View style={styles.tabContent}>
				{renderPage}
			</View>
		)
	}

	render() {
		if(Platform.OS === 'ios'){
		return (
			  <TabBarIOS style={{height: 20}}>
					<TabBarIOS.Item
						title="Team"
						icon={require('./public/icons/add-users.png')}
					  	selected={this.state.selectedTab === 'checkIn'}
					  	onPress={() => {
							this.setState({
						  	selectedTab: 'checkIn',
							})
					  	}}>
					  	{this._renderContent(<CheckIn navigator={this.props.navigator} team={this.state.team} />)}
					  </TabBarIOS.Item>

					<TabBarIOS.Item
					  	title="Games"
							icon={require('./public/icons/soccer.png')}
					  	selected={this.state.selectedTab === 'game'}
						onPress={() => {
							this.setState({
						  	selectedTab: 'game'
							})
					  	}}>
					  	{this._renderContent(<GamePage team={this.state.team}/>)}
					</TabBarIOS.Item>

					<TabBarIOS.Item
					  	renderAsOriginal
					  	title="Results"
					  	selected={this.state.selectedTab === 'blank'}
					  	onPress={() => {
							this.setState({
						  		selectedTab: 'blank'
							})
					  	}}>
					  	{this._renderContent(<Blank/>)}
					</TabBarIOS.Item>
			  </TabBarIOS>
			)
		}//end of ios if
		else if(Platform.OS === 'android'){
			return(
				<ScrollableTabView style={{height: 20}}
					tabBarActiveTextColor="#007F27"
					tabBarUnderlineColor="#007F27"
					>
					<CheckIn navigator={this.props.navigator} team={this.state.team} tabLabel="Team"/>
					<GamePage team={this.state.team} tabLabel="Games"/>
					<Blank tabLabel="Results"/>
 				</ScrollableTabView>
			)
		}
	}
}
