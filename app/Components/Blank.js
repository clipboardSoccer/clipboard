import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	TabBarIOS,
	ScrollView
} from 'react-native';


import db from '../db/db.js';


export default class Blank extends Component {
	constructor(props) {
		super(props)
		this.state = {
		}
	}

	_renderContent(renderPage){
		return (
      <View>
        {renderPage}
      </View>
		)
  }


	render() {
		// debugger;
		return (
      <View style={{bottom: -40}}>
        <Text>This is a blank canvas</Text>
      </View>
    )
	}
}
