import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	TextInput,
	View,
	ScrollView,
	AsyncStorage,
	TouchableHighlight,
	Image,
	Dimensions,
	TabBarIOS,
	NavigatorIOS,
	TouchableWithoutFeedback,
	TabsRoute,
	StatusBar
} from 'react-native';

import Teams from './Teams';

import db from '../db/db.js';

import styles from './public/styles/ChooseTeamStyles';

export default class ChooseTeams extends Component {
	constructor(props) {
		super(props)
		this.state = {
			teams: this.props.teams
		}
	}

	componentWillMount(){
		db.find({Team_Name: {$exists:true}}, function(err, docs){

			this.setState({
				teams: docs
			});

		}.bind(this))
	}

	showTeams(){
		let allTeams = []
		this.state.teams.forEach(function(thisTeam){
			allTeams.push(<Teams
				navigator={this.props.navigator}
				startDate={thisTeam.Start_Date}
				teamName={thisTeam.Team_Name}
				id={thisTeam._id}
				key={thisTeam._id} />
			)
		}.bind(this));

		return(
			<View style={styles.teamsContainer}>
				{allTeams}
			</View>
		)
	}

	chooseTeam(){
		return(
			<ScrollView>
				<View style={styles.chooseTeamsContainer}>
					<Text style={styles.header}>Choose A Team</Text>

					{this.showTeams()}

					<View style={styles.addTeamContainer}>
						<TouchableHighlight
						onPress={ () => this.handleAdd(this.state.teams) }>
							<View>
								<Text style={styles.addNew}>
									Create New Team
								</Text>
							</View>
						</TouchableHighlight>
					</View>
				</View>
			</ScrollView>
		)
	}

	handleAdd(team){
		this.props.navigator.push({
			name: 'CreateTeam',
			passProps: team,
		});
	}
	render() {

			return (
				<View>
					{this.chooseTeam()}
				</View>
			)
	}
}
