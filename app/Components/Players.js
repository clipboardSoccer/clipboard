import React, { Component } from 'react';

import {
	Text,
	TextInput,
	View,
} from 'react-native';

import Swiper from 'react-native-swiper';
import styles from './public/styles/styles.js'

import db from '../db/db.js';
import Player from './Player/Player.js';

export default class Players extends Component {
	constructor(props) {
		super(props)
		this.state = {
			players: ''
		}
	}

	render() {
		if(this.props.players.length > 0) {
			let players = [];
			this.props.players.forEach(function(player){
				players.push(<Player playerFirstName={player.playerFirstName} playerLastName={player.playerLastName} playerNumber={player.playerNumber} Control={player.Control} Passing={player.Passing} Shooting={player.Shooting} Size={player.Size} Speed={player.Speed} key={player._id} id={player._id} editPlayer={this.props.editPlayer} />)
			}.bind(this));

			return(
				<View>
					<Swiper
					showsPagination={true}
					paginationStyle={{ bottom: 5}}
					dot={<View style={{backgroundColor:'rgba(0,0,0,.2)', width: 5, height: 5,borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />}
					activeDot={<View style={{backgroundColor: '#05fb27', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />}
					style={styles.swiper}
					height={180}
					>
						{players}
					</Swiper>
				</View>
			);
		}

		return(
			<View style={styles.noPlayerContainer}>
				<Text style={styles.noPlayer}>
					No Current Players
				</Text>
			</View>
		);
	}
}
