import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	ScrollView,
	PanResponder,
	Animated,
	Dimensions,
	TouchableWithoutFeedback,

} from 'react-native';

import animatedStyles from '../public/styles/animatedStyles';

let ACTION_TIMER = 400;
let COLORS = ['rgb(255,255,255)', 'rgb(111,235,62)'];

export default class MidfieldPlayerThree extends Component {
	constructor(props){
		super(props)
		this.state = {
			player: this.props.player,
			currentPosition: 'midfieldPlayerThree',
			player: this.props.player,
			pressAction: new Animated.Value(0),
			pressed: false,
			buttonWidth: 0,
			buttonHeight: 0
		}
	}

	componentWillMount() {
		this._value = 0;
		this.state.pressAction.addListener((v) => this._value = v.value);
	}

	componentWillReceiveProps(nextProps){
		// debugger;
		this.setState({
			player: nextProps.player
		})
	}

	handlePressIn() {
		// debugger;
		Animated.timing(this.state.pressAction, {
			duration: ACTION_TIMER,
			toValue: 1
		}).start(this.animationActionComplete.bind(this));
	}

	handlePressOut() {
		Animated.timing(this.state.pressAction, {
			duration: this._value * ACTION_TIMER,
			toValue: 0
		}).start();
		
		this.setState({
			pressed: false
		})
	}

	animationActionComplete() {

		if (this._value === 1) {
			this.setState({
				pressed: true
			});
		}

		// debugger;
		this.props.player[0].props._playerSwap(this);
	}
	
	getButtonWidthLayout(e) {
		this.setState({
			buttonWidth: e.nativeEvent.layout.width - 6,
			buttonHeight: e.nativeEvent.layout.height - 6
		});
	}

	getProgressStyles() {
		var width = this.state.pressAction.interpolate({
			inputRange: [0, 1],
			outputRange: [0, this.state.buttonWidth]
		});

		var bgColor = this.state.pressAction.interpolate({
			inputRange: [0, 1],
			outputRange: COLORS
		});
		
		return {
			width: width,
			height: this.state.buttonHeight,
			backgroundColor: bgColor
		}
	}

	render(){
		const pressed = {
			// alignItems: 'center', 
			// justifyContent: 'space-between', 
			// flexDirection: 'row', 
			// margin: 20, 
			// borderColor: 'blue', 
			// borderStyle: 'solid', 
			// borderWidth: 1, 
			backgroundColor: '#7fc37f'
		};
		
		const notPressed = {
			// alignItems: 'center', 
			// justifyContent: 'space-between', 
			// flexDirection: 'row', 
			// margin: 20, 
			// borderColor: 'blue', 
			// borderStyle: 'solid', 
			// borderWidth: 1, 
			backgroundColor: '#c5f753'
		};
		const press = this.state.pressAction._value == true ? pressed : notPressed ;
		// debugger;
		return(
			<View>
				<View style={animatedStyles.aStylesContainer}>
					<TouchableWithoutFeedback 
					onPressIn={this.handlePressIn.bind(this)} 
					onPressOut={this.handlePressOut.bind(this)}
					>
						<View style={[animatedStyles.button, press]} onLayout={this.getButtonWidthLayout}>
							<Animated.View style={[animatedStyles.bgFill, this.getProgressStyles()]} />
							{this.state.player}
						</View>
					</TouchableWithoutFeedback>
					<View>
						<Text>{this.state.textComplete}</Text>
					</View>
				</View>
			</View>
		)
	}

}