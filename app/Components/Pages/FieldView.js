import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	ScrollView,
	PanResponder,
	Animated,
	Dimensions,
	TouchableWithoutFeedback,

} from 'react-native';

import db from '../../db/db.js';
import styles from '../public/styles/gamePageStyles.js';

import ForwardPlayerOne from './ForwardPlayerOne';
import ForwardPlayerTwo from './ForwardPlayerTwo';

import MidfieldPlayerOne from './MidfieldPlayerOne';
import MidfieldPlayerTwo from './MidfieldPlayerTwo';
import MidfieldPlayerThree from './MidfieldPlayerThree';

import DefenderPlayerOne from './DefenderPlayerOne';
import DefenderPlayerTwo from './DefenderPlayerTwo';
import DefenderPlayerThree from './DefenderPlayerThree';

import BenchedPlayer from './BenchedPlayer';

import Button from 'react-native-button';
import GamePageGameStartButton from '../Player/GamePageGameStartButton';

const MAX_NO_OF_STARTERS = 9

var globalRec;



export default class FieldView extends Component {
	constructor(props){
		super(props)
		this.state = {
			update: false,
			players: this.props.players,
			playersSorted: this.props.playersSorted,
			team: this.props.team,
			quarter: 'New Game',
			currentQuarter: 0,
			allCurrentPositions: [],
			currentStarters: [],
			currentBench: [],
			tempArr:[]
		}
		// debugger;
	}

	componentDidMount(){
		// debugger;
	}
	componentWillReceiveProps(object,nextProps){
		// debugger;
		this.state.update = true
		globalRec = object.players
		this.setState({
			players: object.players,
		});

		// this.state.players = object.players;
		this.filterField(globalRec);
	}

	componentWillUpdate(){
		// debugger;
		this.filterField(globalRec);
	}

	shouldComponentUpdate(nextProps, nextState){
		// debugger;
		// globalRec = nextProps.players.length;
		// if(nextState.currentQuarter != 0){
		// 	this.reSortBench(nextState.currentQuarter, nextState.benchedPlayers)
		// }


//==============================================================================
// USE THE IF CONDITION BELOW TO COMPARE AND FORCE A RE-RENDER
//==============================================================================

		if(this.state.update && nextState.update || this.state.players.length != nextProps.players.length || this.state.currentQuarter != nextState.currentQuarter) {
			return true
		} else {
			return false
		}

	}

	componentWillMount(){
		// debugger;
		this.filterField();

	}


	orderCheck(arr){
		// debugger;
		let orderCheck = parseInt(this.state.currentQuarter) % 2;

		if(orderCheck === 0){
				arr.sort(function(a,b){
				return b.props.agg - a.props.agg
			});
		}
		else{
				arr.sort(function(a,b){
				return a.props.agg - b.props.agg
			});
		}

		// debugger;
		return arr;
	}

	filterField(globalRec) {

		let forwardPlayers = [];
		let forwardPlayerOne = [];
		let forwardPlayerTwo = [];

		let midfieldPlayers = [];
		let midfieldPlayerOne = [];
		let midfieldPlayerTwo = [];
		let midfieldPlayerThree = [];

		let defenderPlayers = [];
		let defenderPlayerOne = [];
		let defenderPlayerTwo = [];
		let defenderPlayerThree = [];

		let benchedPlayers = [];
		let lastQtBenchPlayers;

		if(this.state.currentQuarter != 0){
			lastQtBenchPlayers = this.state.benchedPlayers;
		}
		// debugger;
		if(globalRec != undefined){
			var isHereAgg = globalRec
		} else {
			// debugger;
			var isHereAgg = this.state.players;
			isHereAgg = this.orderCheck(isHereAgg)
		}

		// debugger;

		isHereAgg.forEach(function(player, index){ //First for the swaps..

			// debugger;

			if(player.props.nextPosition === 'forwardPlayerOne'){
				forwardPlayers.push(player);
				forwardPlayerOne.push(player);
				isHereAgg.splice(isHereAgg.indexOf(player), 1);

				return
			}

			if(player.props.nextPosition === 'forwardPlayerTwo'){
				forwardPlayers.push(player);
				forwardPlayerTwo.push(player);
				isHereAgg.splice(isHereAgg.indexOf(player), 1);
				return
			}

			if(player.props.nextPosition === 'midfieldPlayerOne'){
				midfieldPlayers.push(player);
				midfieldPlayerOne.push(player);
				isHereAgg.splice(isHereAgg.indexOf(player), 1);
				return
			}

			if(player.props.nextPosition === 'midfieldPlayerTwo'){
				midfieldPlayers.push(player);
				midfieldPlayerTwo.push(player);
				isHereAgg.splice(isHereAgg.indexOf(player), 1);
				return
			}

			if(player.props.nextPosition === 'midfieldPlayerThree'){
				midfieldPlayers.push(player);
				midfieldPlayerThree.push(player);
				isHereAgg.splice(isHereAgg.indexOf(player), 1);
				return
			}

			if(player.props.nextPosition === 'defenderPlayerOne'){
				defenderPlayers.push(player);
				defenderPlayerOne.push(player);
				isHereAgg.splice(isHereAgg.indexOf(player), 1);
				return
			}

			if(player.props.nextPosition === 'defenderPlayerTwo'){
				defenderPlayers.push(player);
				defenderPlayerTwo.push(player);
				isHereAgg.splice(isHereAgg.indexOf(player), 1);
				return
			}

			if(player.props.nextPosition === 'defenderPlayerThree'){
				defenderPlayers.push(player);
				defenderPlayerThree.push(player);
				isHereAgg.splice(isHereAgg.indexOf(player), 1);
				return
			}

			if(player.props.nextPosition === 'benchedPlayers'){
				benched.push(player);
				isHereAgg.splice(isHereAgg.indexOf(player), 1);
				return
			}

		}.bind(this));

		//function to take in the isHereAgg and return re-sorted array


		isHereAgg.forEach(function(player, index){ //then for eveyone else..
			// console.log(player.props.agg);
			// debugger;

			if(player.props.pos1 === "F/W" && forwardPlayers.length < 2) {
				// debugger;

				forwardPlayers.push(player);

				if(forwardPlayerOne.length == 0){
					// debugger;
					player.props.currentPosition
					forwardPlayerOne.push(player);
					// this.updateStartersByQt(player.props._id)
					return
				}

				if (forwardPlayerTwo.length == 0){
					forwardPlayerTwo.push(player)
					// this.updateStartersByQt(player.props._id)
					return
				}

				// return
			}

			if (player.props.pos1 === 'M' && midfieldPlayers.length < 3) {
				// debugger;

				midfieldPlayers.push(player);

				if(midfieldPlayerOne.length == 0){
					midfieldPlayerOne.push(player);
					// this.updateStartersByQt(player.props._id)
					return
				}

				if (midfieldPlayerTwo.length == 0){
					midfieldPlayerTwo.push(player)
					// this.updateStartersByQt(player.props._id)
					return
				}

				if (midfieldPlayerThree.length == 0){
					midfieldPlayerThree.push(player)
					// this.updateStartersByQt(player.props._id)
					return
				}

			}

			if (forwardPlayers.length == 2 && midfieldPlayers.length < 3) {

				midfieldPlayers.push(player);

				if(midfieldPlayerOne.length == 0){
					midfieldPlayerOne.push(player);
					// this.updateStartersByQt(player.props._id)
					return
				}

				if (midfieldPlayerTwo.length == 0){

					midfieldPlayerTwo.push(player)
					// this.updateStartersByQt(player.props._id)
					return
				}

				if (midfieldPlayerThree.length == 0){
					// this.updateStartersByQt(player.props._id)
					midfieldPlayerThree.push(player)
					return
				}
			}

			if (midfieldPlayers.length == 3 && forwardPlayers.length < 2) {
				// debugger;
				forwardPlayers.push(player)

				if(forwardPlayerOne.length == 0){
					// this.updateStartersByQt(player.props._id)
					forwardPlayerOne.push(player);
					return
				}

				if (forwardPlayerTwo.length == 0){
					// this.updateStartersByQt(player.props._id)
					forwardPlayerTwo.push(player)
					return
				}
			}

			if (defenderPlayers.length < 3 ){
				// debugger;
				defenderPlayers.push(player)

				if(defenderPlayerOne.length == 0){
					// this.updateStartersByQt(player.props._id)
					defenderPlayerOne.push(player);
					return
				}

				if (defenderPlayerTwo.length == 0){
					// this.updateStartersByQt(player.props._id)
					defenderPlayerTwo.push(player)
					return
				}

				if (defenderPlayerThree.length == 0){
					// this.updateStartersByQt(player.props._id)
					defenderPlayerThree.push(player)
					return
				}
			}

			if (defenderPlayers.length == 3) {
				// this.updateStartersByQt(player.props._id)
				benchedPlayers.push(<BenchedPlayer player={player} key={player.props._id}/>);
				return
			}
		}.bind(this));

		// debugger;

		this.state.update = false;
		this.state.forwardPlayerOne = forwardPlayerOne;
		this.state.forwardPlayerTwo = forwardPlayerTwo;
		this.state.midfieldPlayerOne = midfieldPlayerOne;
		this.state.midfieldPlayerTwo = midfieldPlayerTwo;
		this.state.midfieldPlayerThree = midfieldPlayerThree;
		this.state.defenderPlayerOne = defenderPlayerOne;
		this.state.defenderPlayerTwo = defenderPlayerTwo;
		this.state.defenderPlayerThree = defenderPlayerThree;
		this.state.benchedPlayers = benchedPlayers;

		// debugger;
		this.setState({
			update: false,
			forwardPlayerOne: forwardPlayerOne,
			forwardPlayerTwo: forwardPlayerTwo,
			midfieldPlayerOne: midfieldPlayerOne,
			midfieldPlayerTwo: midfieldPlayerTwo,
			midfieldPlayerThree: midfieldPlayerThree,
			defenderPlayerOne: defenderPlayerOne,
			defenderPlayerTwo: defenderPlayerTwo,
			defenderPlayerThree: defenderPlayerThree,
			benchedPlayers: benchedPlayers,
			keyFP1: Math.random(),
			keyFP2: Math.random()
		});
		// debugger;

		// if(globalRec != this.state.players.length) {
			// this.forceUpdate();
		// }
		// debugger;
	}//end of filter field

	getQuarter(q){
		// debugger;
		console.log(q);
		this.setState({
			currentStarters: [],
			currentQuarter: q
		})
	}

	getForwardPlayerOneName(){
		if(this.state.forwardPlayerOne.length > 0){
			return (
				<Text style={styles.positionNameText}>
					{this.state.forwardPlayerOne[0].props.playerLastName}
				</Text>
			)
		} else {
			return (
				<Text style={styles.positionNameText}>
					No Player
				</Text>
			)
		}
	}

	getForwardPlayerTwoName(){
		if(this.state.forwardPlayerTwo.length > 0){
			return (
				<Text style={styles.positionNameText}>
					{this.state.forwardPlayerTwo[0].props.playerLastName}
				</Text>
			)
		} else {
			return (
				<Text style={styles.positionNameText}>
					No Player
				</Text>
			)
		}
	}

	getMidfieldPlayerOneName(){
		if(this.state.midfieldPlayerOne.length > 0){
			// debugger;
			return (
				<Text style={styles.positionNameText}>
					{this.state.midfieldPlayerOne[0].props.playerLastName}
				</Text>
			)
		} else {
			return (
				<Text style={styles.positionNameText}>
					No Player
				</Text>
			)
		}
	}

	getMidfieldPlayerTwoName(){
		if(this.state.midfieldPlayerTwo.length > 0){
			return (
				<Text style={styles.positionNameText}>
					{this.state.midfieldPlayerTwo[0].props.playerLastName}
				</Text>
			)
		} else {
			return (
				<Text style={styles.positionNameText}>
					No Player
				</Text>
			)
		}
	}

	getMidfieldPlayerThreeName(){
		if(this.state.midfieldPlayerThree.length > 0){
			return (
				<Text style={styles.positionNameText}>
					{this.state.midfieldPlayerThree[0].props.playerLastName}
				</Text>
			)
		} else {
			return (
				<Text style={styles.positionNameText}>
					No Player
				</Text>
			)
		}
	}

	getDefenderPlayerOneName(){
		// debugger;
		if(this.state.defenderPlayerOne.length > 0){
			return (
				<Text style={styles.positionNameText}>
					{this.state.defenderPlayerOne[0].props.playerLastName}
				</Text>
			)
		} else {
			return (
				<Text style={styles.positionNameText}>
					No Player
				</Text>
			)
		}
	}

	getDefenderPlayerTwoName(){
		if(this.state.defenderPlayerTwo.length > 0){
			return (
				<Text style={styles.positionNameText}>
					{this.state.defenderPlayerTwo[0].props.playerLastName}
				</Text>
			)
		} else {
			return (
				<Text style={styles.positionNameText}>
					No Player
				</Text>
			)
		}
	}
	getDefenderPlayerThreeName(){
		if(this.state.defenderPlayerThree.length > 0){
			return (
				<Text style={styles.positionNameText}>
					{this.state.defenderPlayerThree[0].props.playerLastName}
				</Text>
			)
		} else {
			return (
				<Text style={styles.positionNameText}>
					No Player
				</Text>
			)
		}
	}
	getBenchedPlayerNames(){
		// debugger;
		if(this.state.benchedPlayers.length > 0){
			let playersOnBench = []
			// debugger;
			this.state.benchedPlayers.forEach(function(player, index){
				// debugger;
				playersOnBench.push(
					<Text style={styles.positionNameText}key={player.props.player.props._id}>{player.props.player.props.playerLastName}</Text>
				)
			}.bind(this));

			return (
				<View>
					{playersOnBench}
				</View>
			)

		} else {
			return (
				<Text style={styles.positionNameText}>
					No Player
				</Text>
			)
		}
	}



	render() {
		// debugger;

		return (

			<View style={styles.fieldPage}>
				<View style={styles.playingField}>
					<View style={styles.mainContainer}>
						<View style={styles.positionContainer}>
							<ForwardPlayerOne player={this.state.forwardPlayerOne} key={this.state.keyFP1}/>
							<ForwardPlayerTwo player={this.state.forwardPlayerTwo} key={this.state.keyFP2}/>
						</View>

						<View style={styles.positionContainer}>
							<MidfieldPlayerOne player={this.state.midfieldPlayerOne} />
							<MidfieldPlayerTwo player={this.state.midfieldPlayerTwo} />
							<MidfieldPlayerThree player={this.state.midfieldPlayerThree} />
						</View>

						<View style={styles.positionContainer}>
							<DefenderPlayerOne player={this.state.defenderPlayerOne} />
							<DefenderPlayerTwo player={this.state.defenderPlayerTwo} />
							<DefenderPlayerThree player={this.state.defenderPlayerThree} />
						</View>

						<View style={styles.benchedContainer}>
							{this.state.benchedPlayers}
						</View>
					</View>
					<View style={styles.playerPositionContainer}>
						<View style={styles.positionNames}>
							{this.getForwardPlayerOneName()}
							{this.getForwardPlayerTwoName()}
						</View>

						<View style={styles.positionNames}>
							{this.getMidfieldPlayerOneName()}
							{this.getMidfieldPlayerTwoName()}
							{this.getMidfieldPlayerThreeName()}
						</View>

						<View style={styles.positionNames}>
							{this.getDefenderPlayerOneName()}
							{this.getDefenderPlayerTwoName()}
							{this.getDefenderPlayerThreeName()}
						</View>
						<View style={styles.benchedPositionNames}>
							{this.getBenchedPlayerNames()}
						</View>
					</View>
				</View>
				<GamePageGameStartButton getQuarter={this.getQuarter.bind(this)} filterField={this.filterField.bind(this)} />
			</View>
		)
	}

}
