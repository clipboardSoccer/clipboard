import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	ScrollView,
	PanResponder,
	Animated,
	Dimensions,
} from 'react-native';

import db from '../../db/db.js';
import styles from '../public/styles/gamePageStyles.js';
// import GamePagePlayerButton from '../Player/GamePagePlayerButton';
import ForwardDropZone from './ForwardDropZone';
import MidfieldDropZone from './MidfieldDropZone';
import DefenderDropZone from './DefenderDropZone';
import KeeperDropZone from './KeeperDropZone';


export default class DraggablePlayer extends Component {
	constructor(props){
		super(props)
			this.state = {
				player: this.props.player,
				showDraggable   : true,     
				dropZoneValues  : this.props.dropZoneValues,
				pan     : new Animated.ValueXY(),
			}
	}

	componentWillMount(){
		// debugger;
		const getThis = this;

		this.panResponder = PanResponder.create({ 
			onStartShouldSetPanResponder: () => true,
			onPanResponderMove: Animated.event([null,{ 
				dx : this.state.pan.x,
				dy : this.state.pan.y
			}]),
			onPanResponderRelease: function(e, gesture) {
				debugger;
				if (getThis.isDropZone(gesture)){ 
					console.log(this.state.dropZoneValues)
				} else {
					Animated.spring(
						this.state.pan,
						{toValue:{x:0,y:0}}
					).start();
				}
			}
		});
	}

	// componentDidUpdate(){
	// 	// debugger; //inside DraggablePlayer check for this.props.dropZoneValues
	// 	debugger;
	// 	if(this.props.dropZoneValues.y){
	// 		this.state.dropZoneValues = this.props.dropZoneValues
	// 	}
	// }



	//*********************** KEEP THIS *****************************************
	//																			*
	//	<Animated.View>															*
	// 		<Text style={styles.gpHeading}>{this.props.team.teamName}</Text>	*
	// 			{playersArr}													*
	// </Animated.View>															*
	//																			*																		*
	//*********************** KEEP THIS *****************************************

	renderDraggable(){
		// debugger; //inside DraggablePlayer
		if(this.state.showDraggable){

			// debugger;

			return(
				<View style={styles.allPlayersContainer}>
					<View 
					style={styles.draggableContainer}>
						<Animated.View 
						{...this.panResponder.panHandlers}
						style={[this.state.pan.getLayout(), styles.player]}>
							<Text style={styles.text}>{this.state.player.playerFirstName}</Text>
						</Animated.View>
					</View>
				</View>
			)
		}
	}

	// setDropZoneValues(event){
	// 	this.setState({
	// 		dropZoneValues : event.nativeEvent.layout
	// 	});
	// 	// debugger;
	// }

	isDropZone(gesture){ 
		var dz = this.state.dropZoneValues;
		debugger;
		return gesture.moveY > dz.y && gesture.moveY < (dz.y + dz.height);
	}

	render(){
		console.log('state: ', this.state)

		return (
			<View>
				{this.renderDraggable()}
			</View>
		)
	}
}

