import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	ScrollView,
	PanResponder,
	Animated,
	Dimensions,
} from 'react-native';

import db from '../../db/db.js';
import styles from '../public/styles/gamePageStyles.js';
import PlayerPen from './PlayerPen';


export default class KeeperDropZone extends Component {
	constructor(props){
		super(props)
			this.state = {
				playersArr: this.props.playersArr, 
				ForwardDropZoneValues: this.props.ForwardDropZoneValues,
				MidfieldDropZoneValues: this.props.MidfieldDropZoneValues,
				DefenderDropZoneValues: this.props.DefenderDropZoneValues,   
				KeeperDropZoneValues: null,
			}
	}
	
	setDropZoneValues(event){
		// debugger;
		this.setState({
			KeeperDropZoneValues : event.nativeEvent.layout
		});
		
	}

	render(){
		console.log('state: ', this.state)

		return (
			<View>
				<View style={styles.mainContainer}>

					<View 
					onLayout={this.setDropZoneValues.bind(this)}
					style={styles.keeper}>
						<Text style={styles.text}>keeper</Text>
					</View>	

				</View>

				<PlayerPen playersArr={this.state.playersArr} ForwardDropZoneValues={this.state.ForwardDropZoneValues} MidfieldDropZoneValues={this.state.MidfieldDropZoneValues} DefenderDropZoneValues={this.state.DefenderDropZoneValues} KeeperDropZoneValues={this.state.KeeperDropZoneValues}/>
			</View>
		)
	}
}