import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	ScrollView,
	PanResponder,
	Animated,
	Dimensions,
} from 'react-native';

import db from '../../db/db.js';
import styles from '../public/styles/gamePageStyles.js';
// import GamePagePlayerButton from '../Player/GamePagePlayerButton';
import DraggablePlayer from './DraggablePlayer';

export default class PlayerPen extends Component {
	constructor(props){
		super(props)
			this.state = {
				playersArr: this.props.playersArr,
				// showDraggable   : true,     
				dropZoneValues  : {
					ForwardDropZoneValues: this.props.ForwardDropZoneValues,
					MidfieldDropZoneValues: this.props.MidfieldDropZoneValues,
					DefenderDropZoneValues: this.props.DefenderDropZoneValues,
					KeeperDropZoneValues: this.props.KeeperDropZoneValues
				}
				// pan     : new Animated.ValueXY(),
			}
	}

	// componentWillRecieveProps(){

	// }

	renderPlayers(){
		// debugger;// Is this stopping here?
		// if(this.state.showDraggable){
		let players = []

		this.state.playersArr.forEach(function(player){
			// debugger; //stop here

			let value = this.props.dropZoneValues

			players.push(
				<DraggablePlayer player={player.props} dropZoneValues={this.state.dropZoneValues} key={player.props.playerId} />
			)			
		}.bind(this));

		// debugger;

		return(
			<View style={styles.allPlayersContainer}>
				{players}
			</View>
		)
	}

	render(){
		console.log('state playersPen: ', this.state)
		// debugger;
		return (
			<View>
				{this.renderPlayers()}
			</View>							
		);
	}
}

