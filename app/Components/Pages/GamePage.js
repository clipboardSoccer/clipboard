import React, { Component } from 'react';
import {
	Text,
	View,
	TouchableHighlight,
	ScrollView,
	Image,
} from 'react-native';

import db from '../../db/db.js';
import styles from '../public/styles/gamePageStyles.js';
import GamePagePlayerButton from '../Player/GamePagePlayerButton';
import FieldView from './FieldView';
import net from '../../../public/images/inflicted.png';


export default class GamePage extends Component {
	constructor(props){
		super(props)
			this.state = {
				players: [],
				team: this.props.team,
				playerSwap: 0,
				swapIds: {},
				quarterCounter: '',
				currentQuarter: ''
			}
	}

	componentWillReceiveProps(object){
		object.team.players.forEach(function(player, index){
			this.state.players[index].isHere = player.isHere
		}.bind(this));
	}

	componentWillMount() {
		this.playerSort();
	}

	playerSort(){
		const keeper = 'G';
		const forward = 'F/W';
		const midfield = 'M';
		const defender = 'D';
		const FACTORS = 4;
		let sortByAgg;
		let players = [];
		let toSort = [];
		let keepUnSorted = [];
		let firstPos, secondPos, thirdPos;
		let counter = 0;

		if(this.state.team.players.length > 0){
			this.state.team.players.forEach(function(player){
				let forwardVal = parseInt(player.Speed) + parseInt(player.Shooting);
				let midfieldVal = parseInt(player.Control) + parseInt(player.Passing);
				let aggregate = (forwardVal + midfieldVal)/FACTORS;

				if(forwardVal >= midfieldVal){
					firstPos = forward;
					secondPos = midfield;
					thirdPos = defender;
				}
				else if(midfieldVal > forwardVal){
					firstPos = midfield;
					secondPos = forward;
					thirdPos = defender;
				}

				let playerObj = {
					isHere: player.isHere,
					_id: player._id,
					teamId: player.teamId,
					teamName: this.props.team.teamName,
					playerNumber: player.playerNumber,
					playerFirstName: player.playerFirstName,
					playerLastName: player.playerLastName,
					Speed: parseInt(player.Speed),
					Shooting: parseInt(player.Shooting),
					Control: parseInt(player.Control),
					Passing: parseInt(player.Passing),
					pos1: firstPos,
					pos2: secondPos,
					pos3: thirdPos,
					quartersPlayed: 0,
					agg: Math.round(aggregate).toFixed(2)
				}

				toSort.push(playerObj)
				keepUnSorted.push(playerObj);
				sortByAgg = toSort.slice(0)
				sortByAgg.sort(function(a,b){
					return b.agg - a.agg

				});

				counter++
			}.bind(this))//end of forEach


			if(counter === this.state.team.players.length){

				this.setState({
					players: keepUnSorted,
					playersSorted: sortByAgg,
				});
			}
		}//end of this.state.players.length if
	}

	_playerSwap(cb){
		// debugger;
		let swap = this.state.playerSwap;
		swap++
		this.state.playerSwap = swap


		if(cb.state.player[0]) {

			var id = {
				id: cb.state.player[0].key,
				position: cb.state.currentPosition
			}
		} else if (cb.state.player.key) {

			var id = {
				id: cb.state.player.key,
				position: cb.state.currentPosition
			}
		}

		if(this.state.playerSwap < 2){

			this.setState({
				playerSwap: swap,
				swapIds: id,
			});
		}

		if(this.state.playerSwap == 2){

			let firstSwap = this.state.swapIds;
			let firstIndex = 0
			let secondSwap = id;
			let secondIndex = 0

			this.state.players.forEach(function(player, index){

				if (player._id == firstSwap.id){
					secondIndex = index
				}

				if(player._id == secondSwap.id){
					firstIndex = index
				}

			}.bind(this));

			let tempPlayer = this.state.players;
			tempPlayer[firstIndex].nextPosition = firstSwap.position;
			tempPlayer[secondIndex].nextPosition = secondSwap.position;

			this.setState({
				players: tempPlayer,
				playerSwap: 0
			});
		}
	}

	render(){
		let playersArr = [];

		if(this.state.players.length != 0){

			this.state.players.forEach(function(player){

				if(player.isHere){
					playersArr.push(
						<GamePagePlayerButton
							_playerSwap={this._playerSwap.bind(this)}
							key={player._id}
							_id={player._id}
							teamId={player.teamId}
							playerNumber={player.playerNumber}
							playerFirstName={player.playerFirstName}
							playerLastName={player.playerLastName}
							Speed={player.Speed}
							Control={player.Control}
							Passing={player.Passing}
							Shooting={player.Shooting}
							pos1={player.pos1}
							pos2={player.pos2}
							pos3={player.pos3}
							agg={player.agg}
							quartersPlayed={0}
							nextPosition={player.nextPosition}
						/>
					)
				}
			}.bind(this));

			this.state.activePlayers = playersArr
			// debugger;
			return (

				<ScrollView style={styles.gpPageContainer}>
					<View style={styles.gpHeadingContainer}>
						<Text style={styles.gpHeading}>{this.props.team.teamName}</Text>
					</View>
						<FieldView playersSorted={this.state.playersSorted} players={this.state.activePlayers} team={this.state.team} update={true}/>
				</ScrollView>
			)
		} else {
			return(
				<View>
					<Text style={styles.gpHeading}>
						No Current Players
					</Text>
				</View>
			)
		}
	}
}
