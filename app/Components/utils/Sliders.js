
import React, { Component } from 'react';
import appStyles from '../public/styles/SlidersStyle'

import {
	StyleSheet,
	Text,
	View,
	ScrollView
} from 'react-native';

import {
	MKColor,
	MKSlider,
	MKRangeSlider,
	setTheme
} from 'react-native-material-kit';

// import styles from '../public/styles/SlidersStyle';

const styles = Object.assign({}, appStyles, StyleSheet.create({
	slider: {
		width: 200,
	},
}));

export class ValueText extends Component {
	constructor(props) {
		super(props);
		this.state = {
			curValue: props.initial,
		};
	}

	onChange(curValue) {
		this.setState({curValue});
		this.props.curValue = curValue;
	}

	render() {
		return (
			<Text style={styles.legendLabel}>
				{this.state.curValue} ({this.props.rangeText})
			</Text>
		);
	}
}

export default class Sliders extends Component {

	componentDidMount() {
		const slider = this.refs.sliderWithValue;
	}

	render() {
		// debugger;
		return (
			<View>
				<View style={styles.row}>
					<View style={styles.row}>
						<View style={styles.row}>
							<Text style={styles.type}>
								{this.props.type}
							</Text>
							<MKSlider
								ref="sliderWithValue"
								min={1}
								max={100}
								value={5}
								style={styles.slider}
								onChange={(curValue) => this.refs.valueText.onChange(curValue.toFixed(0))}
							/>
							<ValueText ref="valueText" initial="5" rangeText="1-100" />

						</View>
					</View>
				</View>
			</View>
		);
	}
}
