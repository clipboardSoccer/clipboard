'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
let CIRCLE_RADIUS = 36;
let Window = Dimensions.get('window');

module.exports = StyleSheet.create({

//*************************** Welcome ***************************

	welcomeContainer: {
		backgroundColor: '#a7e5ff',
		width: screenWidth,
		height: screenHeight,
	},
	imageContainer: {
		// borderWidth: 1,
		borderTopWidth: 4,
		borderTopColor: '#b1e4d9',
		position: 'absolute',
		bottom: 0,
		// tansform: [{scaleX: 0.5}]
		// transform: [{perspective: 1}]
	},

	backgroundImage: {
		// flex: 1,
		// resizeMode: 'center',
		position: 'relative',
		backgroundColor: '#046304',
		// height: screenHeight,
		width: screenWidth,
		// height: 350,
		opacity: .9,

	},

	welcomeScreen: {
		height: screenHeight,
		flexDirection: 'column',
		alignItems: 'center',
	},

	banner: {
		fontSize: 48,
		fontWeight: 'bold',
		textAlign: 'center',
		marginTop: 100,
		backgroundColor: (255,255,255, .0),
		color: '#e1f6ff',
		textShadowColor: '#000',
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 5,
	},

	click: {
		// flex: 1,
		// backgroundColor: '#6bcc66',
		shadowColor: '#333',
		// padding: 50,
		// borderWidth: 1,
		borderRadius: 275,
		justifyContent: 'center',
		// position: 'absolute',

	},

	soccerBall: {
		margin: 25,
		width: 225,
		height: 225,
		justifyContent: 'center',
		bottom: (Platform.OS === 'ios') ? -100 : -25,
    // position: 'absolute'

	},

	getStarted: {
		fontSize: 48,
		textAlign: 'center',
		color: '#04ff3e',
		textShadowColor: '#000',
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 5,
		// backgroundColor: '#6bcc66',
		// height: 100,
	}

});
