'use strict';

import React,
{ Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;

module.exports = StyleSheet.create({

	pageContainer: {
		backgroundColor: '#346d03',
		width: screenWidth,
		height: screenHeight,
		position: 'relative',
	},

	back: {
		textAlign: 'right',
		margin: 10,
		color: '#aaa',
	},

	header: {
		fontWeight: 'bold',
		color: '#ddd',
		textShadowColor: '#222',
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 5, 
		textAlign: 'center', 
		marginTop: 20,	
		fontSize: 36,	
	},

	input: {
		height: 40,
		borderColor: 'gray',
		borderWidth: 1,
		marginTop: 50, 
		margin: 10,
		padding: 10,
	},


	submitContainer: {
		justifyContent: 'center',
		borderWidth: 1,
		height: 125,
		width: screenWidth,
		bottom: 0,
		position: 'absolute',
		backgroundColor: '#ddd',
	},

	submitText: {
		textAlign: 'center',
		fontSize: 48,
		color: '#5ce600',
		textShadowColor: '#222',
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 5, 
	},

});