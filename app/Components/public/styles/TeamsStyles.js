'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
// let Window = Dimensions.get('window');

module.exports = StyleSheet.create({
	teamsPageContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: 5,
		margin: 5,
		backgroundColor: '#01ab09',
		borderRadius: 15,
	},

	teamContainer: {
		borderWidth: 1,
		borderRadius: 25,
		height: (Platform.OS === 'ios') ? 60 : 55,
		width: 275,
		flexDirection: 'column',
		backgroundColor: '#ddd',
		// borderRadius: 120,
		// alignSelf: 'stretch',
		overflow: 'hidden',
		borderColor: 'green',
		// flex: 1
	},
	selectTeam: {

	},
	teamName: {
		fontSize: 36,
		fontWeight: 'bold',
		textAlign: 'center',
		// justifyContent: 'center',
		padding: 10,
		color: '#333',
	},

	removeContainer: {
		// borderWidth: 1,
		// borderRadius: 25,
		height: 30,
		width: 30,
		flexDirection: 'column',
		// backgroundColor: 'purple',
		borderRadius: 30,
		margin: 5,
		justifyContent: 'center',
		// alignSelf: 'stretch',
		overflow: 'hidden',
		borderWidth: 1,
		backgroundColor: '#e6959f',
		// marginRight: 10,
		// borderColor: 'green',
		// flex: 1
	},

	selectRemoveText: {
		textAlign: 'center',
		color: '#ff0000'
	},
});
