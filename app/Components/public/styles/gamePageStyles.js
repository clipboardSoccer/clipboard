'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
// let Window = Dimensions.get('window');

module.exports = StyleSheet.create({


//*************************** Playing Field ***************************
	fieldPage: {
		// flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		// alignItems: 'flex-start',
		// borderWidth: 1,
		// borderColor: 'black',
		// padding: 5,
	},

	playingField: {
		// flex: 1,
		// flexWrap: 'wrap',
		borderColor: '#fff',
		borderStyle: 'solid',
		borderWidth: 3,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'flex-start',
		padding: 10,
		margin: 10,
		backgroundColor: '#4ea900',
		// width: screenWidth
	},

	quarter: {
		textAlign: 'center',
	},

	buttonContainer: {
		alignItems: 'flex-start',
		// flex: 1,
		flexDirection: 'row',
		width: 50,
		height: 50,
		margin: 5,
		padding: 2,
		// margin: 10,
		justifyContent: 'center',
		borderRadius: 10,
		borderWidth: 2,
		borderStyle: 'solid',
		borderColor: 'orange',
		backgroundColor: '#8ee4ea',
	},
	buttons: {
		color: '#02bdca',
		padding: 2,
		width: 35,
		height: 25,
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'row',
		// backgroundColor: '#bdfbff',
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: '#018088',
		// fontSize: 14,
		borderRadius: 5,
	},

	buttonText: {
		// backgroundColor: '#bdfbff',
		// borderWidth: 2,
		// borderStyle: 'solid',
		// borderColor: '#018088',
		// // fontSize: 14,
		// borderRadius: 3,
		// padding: 3,
	},

	gpPageContainer: {
		flex: 1,
		position: 'relative',
		// height: screenHeight,
		backgroundColor: '#346d03',
	},

	gpPlayerContainer: {
		flex: 1,
		flexDirection: 'row',
		backgroundColor: '#333',
		alignItems: 'stretch',
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'green',
		width: screenWidth / 3,
		padding: 5,
		margin: 15
	},

	gpPlayer: {
		// textAlign: 'left',
		// flexDirection: 'row'
	},

	gpButton:{
		flexDirection:'row',
		flexWrap: 'wrap',
		alignItems: 'center',
		justifyContent: 'center',
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'black',
		width: screenWidth / 4,
		padding: 5,
	},

	gpHeadingContainer: {
		borderColor: '#fff',
		borderWidth: 2,
		borderBottomWidth: 4,
		borderTopLeftRadius: 36,
		borderTopRightRadius: 36,
		justifyContent: 'center',
		// transform: [{perspective: 250}],
		paddingTop: 10,
		paddingLeft: 10,
		paddingRight: 10,
		backgroundColor: 'rgba(255,255,255,.1)',
		flexDirection: 'row',
		margin: 10,
	},

	goalNet: {
		// resizeMode: 'contain',
		justifyContent: 'center',
		width: 110,
		borderTopLeftRadius: 36,
	},

	gpHeading: {
		// position: 'absolute',
		margin: 10,
		fontWeight: 'bold',
		color: '#ddd',
		textShadowColor: '#222',
		// borderTopLeftRadius: 36,
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 5,
		textAlign: 'center',
		marginTop: 20,
		fontSize: 36,
	},

	gpModalText: {
		// flex: 1,
		textAlign: 'center',
		padding: 20,
		// color: 'red',
	},
	gpModalContainer: {
		flex: 1,
		flexDirection: 'column',
		// justifyContent: 'center',
		height: screenHeight,
		width: screenWidth,
		alignItems: 'center',
		backgroundColor: 'rgba(0, 0, 0, 0.5)'
	},
	gpModalView: {
		flex: 1,
		flexDirection: 'column',
		// justifyContent: 'center',
		alignItems: 'center',
	},
	gpAttrRow:{
		flex:1,
		justifyContent: 'center',
		flexDirection: 'row',
		alignItems:'center',
	},
	gpModalAttrButton: {
		padding:10,
		height:45,
		width:45,
		overflow:'hidden',
		borderRadius:4,
		backgroundColor: 'white',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		// alignItems: 'center',
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'black',
		margin: 20
	},
	gpModalCloseButton: {
		bottom: 80
	},

	mainContainer: {
		// flex: 1,
		// flexWrap: 'wrap',
		backgroundColor: '#4b6b02',
		borderColor: '#99ff00',
		borderStyle: 'solid',
		borderWidth: 1,
		flexDirection: 'column',
		// justifyContent: 'center',
		width: screenWidth * .6,
	},

	playerPositionContainer: {
		// flex: 1,
		// flexWrap: 'wrap',
		backgroundColor: '#4b6b02',
		borderColor: '#99ff00',
		borderStyle: 'solid',
		borderWidth: 1,
		flexDirection: 'column',
		// justifyContent: 'center',
		width: screenWidth * .25,
	},

	positionContainer: {
		// flex: 1,
		// flexWrap: 'wrap',
		borderColor: 'green',
		margin: 2,
		borderRadius: 10,
		borderStyle: 'solid',
		borderWidth: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		height: 80,
		backgroundColor: '#61d001',
	},

	positionNames: {
		// flex: 1,
		// flexWrap: 'wrap',
		borderRadius: 10,
		borderColor: '#61d001',
		borderStyle: 'solid',
		borderWidth: 1,
		margin: 2,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		height: 80,
		backgroundColor: '#9ad001',

	},

	positionNameText: {
		color: '#ebff00',
		fontSize: 14,
		textAlign: 'center',
	},

	benchedContainer: {
		// flex: 1,
		flexWrap: 'wrap',
		backgroundColor: '#448c09',
		borderRadius: 10,
		borderColor: '#61d001',
		borderStyle: 'solid',
		borderWidth: 1,
		margin: 2,
		// padding: 5,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},

	benchedPositionNames: {
		// flex: 1,
		flexWrap: 'wrap',
		// textAlign: 'center',
		borderRadius: 10,
		borderColor: 'green',
		borderStyle: 'solid',
		borderWidth: 1,
		margin: 2,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#9ad001',
		padding: 10,
	},

});
