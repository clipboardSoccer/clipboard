'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
// let Window = Dimensions.get('window');

module.exports = StyleSheet.create({

	gamePageButtonMainContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		bottom: 0,
		alignItems: 'center',
		margin: 10,
		borderWidth: 3,
		borderBottomLeftRadius: 30,
		borderBottomRightRadius: 30,
		borderColor: '#fff',
		backgroundColor: '#4ea900',
		padding: 20,


	},

	startButtonContainer: {
		padding: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		// margin: 15,
	},

	startQuarterButton: {
		flex: 1,
		width: 175,
		height: 100,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'orange',
		borderWidth: 2,
		borderColor: '#018088',
		borderRadius: 10,
		padding: 10,
	},

	startQuarterText: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		backgroundColor: 'orange',
		color:'#018088',
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: 24,
		
	},

	quarterInfoContainer: {
		borderWidth: 2,
		borderColor: '#018088',
		borderRadius: 10,
		// margin: 15,
		backgroundColor: '#efeb00',
		height: 100,
		width: 100,
		justifyContent: 'center',
	},

	quarterContainer: {
		margin: 3,
		// borderWidth: 1,
		// textAlign: 'center'
	},

	quarterText: {
		fontSize: 18,
		color: '#442e21',
		fontWeight: 'bold',
		textAlign: 'center',

	},

	remainingContainer: {
		margin: 3,
		// borderWidth: 1,

	},

	remainingText: {
		color: '#442e21',
		fontWeight: 'bold',
		textAlign: 'center',
	},


});