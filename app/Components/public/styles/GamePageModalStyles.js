'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
// let Window = Dimensions.get('window');

module.exports = StyleSheet.create({
	gpModalContainer: {
    // borderWidth: 1,
    // borderRadius: 15,
    flexDirection: 'row',
		flex: 1,
    alignItems: 'center',
    padding: 5,
		// margin: 5,
    justifyContent: 'center',
    backgroundColor: 'rgba(52,109,3, 0.95)'
		// flexDirection: 'column',
		// backgroundColor: 'rgba(0, 0, 0, 0.5)'
	},
  gpModalAttrRow:{
    padding: 5,
    margin: 5,
    // borderWidth:1,
    // borderRadius: 25,
    overflow: 'hidden',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: (Platform.OS === 'ios') ? 60 : 55,
    width: 275,
  },
	gpModalAttrButton: {
        // flex: 1,
		padding:10,
		height:50,
		width:50,
		// overflow:'hidden',
		borderRadius: 50,
		backgroundColor: '#ddd',
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: '#222',
		margin: 20
	},
	gpModalCloseButton: {
    // padding: 5,
    marginTop: 175,
    borderWidth:1,
    borderRadius: 25,
    overflow: 'hidden',
    flexDirection: 'row',
    alignItems: 'center',
    height: (Platform.OS === 'ios') ? 60 : 55,
    width: 275,
		bottom: 0,
		backgroundColor: '#ddd'
	},
  gpModalAttrText: {
    // flex: 1,
    justifyContent:'center',
    width: 100,
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#ddd',
  },
  gpModalCloseText: {
    width: 275,
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#222',
  },
  buttonContainer: {
    alignItems: 'flex-start',
    // flex: 1,
    flexDirection: 'row',
    width: 50,
    height: 50,
    margin: 5,
    padding: 2,
    // margin: 10,
    justifyContent: 'center',
    borderRadius: 10,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: 'orange',
    backgroundColor: '#8ee4ea',
  },
  buttons: {
    color: '#02bdca',
    padding: 2,
    width: 35,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    // backgroundColor: '#bdfbff',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#018088',
    // fontSize: 14,
    borderRadius: 5,
  },
  gpModalAttrToggleText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: (Platform.OS === 'ios') ? 24 : 18,
    color: '#222',
    // borderWidth:1,

  },
  aBuffer: {
    height: 0
  }
});
