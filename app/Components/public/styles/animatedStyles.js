'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
let CIRCLE_RADIUS = 75;
// let Window = Dimensions.get('window');

module.exports = StyleSheet.create({


//*************************** Animated Button ***************************
  aStylesContainer: {
    // flex: 1,
    // borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    margin: 5,
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#c5f753'
  },
  text: {
    backgroundColor: 'transparent',
    color: '#111'
  },
  bgFill: {
    position: 'absolute',
    top: 0,
    left: 0
  }

});
