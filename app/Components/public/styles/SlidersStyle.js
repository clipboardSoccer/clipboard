'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
// let Window = Dimensions.get('window');

module.exports = StyleSheet.create({
	// slider: {
	// 	width: 200,
	// },
	slider: {
		flexDirection: 'row',
	 	width: 200,
	 	color: '#333',
	},
	scrollView: {
		flex: 1,
	},
	row: {
		flex: 1,
		flexDirection: 'row',
		// borderWidth: 1,
		justifyContent: 'center',
		alignItems: 'center',
		// width: screenWidth,

	},
	col: {
		// flex: 1,
		flexDirection: 'column',
		marginLeft: 7, marginRight: 7,
	},
	type: {
		color: '#333',
		fontWeight: '800',
	},
	legendLabel: {
		textAlign: 'left',
		color: '#000',
		// marginTop: 10, marginBottom: 20,
		fontSize: 12,
		fontWeight: '300',
	},
	border: {
		// borderWidth: 1,
		// borderStyle: 'solid'
	},
	button: {
		color: '#ccc',
		alignItems: 'center'
	},
});