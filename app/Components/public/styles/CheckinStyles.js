'use strict';

import React,
{ Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;

module.exports = StyleSheet.create({

	checkinPageContainer: {
		backgroundColor: '#346d03',
		height: screenHeight,
	},

	headerButtonContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',

	},

	back: {
		margin: 20,
		color: '#22edfb',
	},

	editTeam: {
		margin: 20,
		color: '#22edfb',
	},

	playerNameContainer: {
		width: 150,
		flexDirection: 'column',
		borderWidth: 1,
		margin: 5,
		borderRadius: 10,
		backgroundColor: '#00efff'
	},

	playerFirstName: {
		margin: 5,
		fontSize: 18,
		textAlign: 'center',
		color: '#ff0000',
		fontWeight: 'bold',
	},

	playerLastName: {
		margin: 5,
		fontSize: 14,
		textAlign: 'center',
		color: '#ff0000',
		fontWeight: 'bold',
	},

	playerNumberContainer: {
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'blue',
		borderRadius: 100,
		width: 45,
		height: 45,
		// margin: 5,
		justifyContent: 'center',
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: '#00efff',
	},

	playerNumber: {
		fontSize: 24,
		color: 'red',
	},

	buttonContainer: {
		flexDirection: 'row',
		borderTopLeftRadius: 15,
		borderBottomLeftRadius: 15,
		borderTopRightRadius: 5,
		borderBottomRightRadius: 5,
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'blue',
		marginLeft: 5,
		backgroundColor: '#83ebf3',
	},

	hereContainer: {
		flexDirection: 'column',
		justifyContent: 'center',
	},

	hereText: {
		margin:5,
		color: '#017907',
	},

	undo: {
		fontSize: 12,
		marginLeft: 15,
		margin: 5,
		padding: 5,
		backgroundColor:'blue',
		borderRadius: 10,
		justifyContent: 'center',
		alignItems: 'center',
		color: '#ff0000',
	},
	aBuffer: {
		height: 50
	}
});
