'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
// let Window = Dimensions.get('window');

module.exports = StyleSheet.create({

	tabContent: {
		borderColor: '#fff',
		borderWidth: 1,
		borderStyle: 'solid',
		flex: 1,
		position: 'relative',
  	},
	tabText: {
		color: 'white',
		marginTop: 20,
		textAlign: 'center',
	},
});