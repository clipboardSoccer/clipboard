'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
// let Window = Dimensions.get('window');

module.exports = StyleSheet.create({

	pageContainer: {
		flex: 1,
		position: 'relative',
		backgroundColor: '#22edfb',
		height: screenHeight,

  	},

	header: {
		fontWeight: 'bold',
		color: '#ddd',
		textShadowColor: '#222',
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 5,
		textAlign: 'center',
		marginBottom: 10,
		marginTop: 5,
		fontSize: 36,
	},

	textInputContainer: {
		borderWidth: 1,
		marginLeft: 5,
		marginRight: 5,
		backgroundColor: '#3f51b5',
		borderRadius: 10,
	},

	textInput: {
		height: 40,
		borderColor: '#22edfb',
		borderWidth: 1,
		margin: 10,
		padding: 10,
		color: 'red',
	},

	sliderContainer: {
		borderWidth: 1,
		margin: 5,
		backgroundColor: '#bae5e8',
		borderRadius: 10,
	},

	buttonContainer: {
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center',
		// borderWidth: 1,
	},

	submitButton: {
		// borderWidth: 1,
		// borderStyle: 'solid',
		// borderColor: 'black',
		width: 100,
		margin: 5,
	},
	removeButton: {
		// borderWidth: 1,
		// borderStyle: 'solid',
		// borderColor: 'black',
		width: 100,
		margin: 5,
	},

	players: {
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'black',
		// height: 200,
		// alignItems: 'center',
		height: (Platform.OS === 'android') ? 190 : null,
		width: screenWidth,
		flexDirection: 'row',
		backgroundColor: '#333',
		position: 'relative',
		// justifyContent: 'center'

	},

	enterGame: {
		// position: 'absolute',
		backgroundColor: '#aaa',
		// bottom:0,
		// borderWidth: 1,
		// borderStyle: 'solid',
		// borderColor: 'red',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		height: 75,
		width: screenWidth,
		fontSize: 36,
		paddingTop: 3,
		bottom: (Platform.OS === 'android') ? 5 : null
	},


});
