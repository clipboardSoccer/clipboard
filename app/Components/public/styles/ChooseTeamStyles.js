'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
// let Window = Dimensions.get('window');

module.exports = StyleSheet.create({

	chooseTeamsContainer: {
		// borderWidth: 3,
		flex: 1,
		backgroundColor: '#346d03',
		height: screenHeight,
		// width: screenWidth,
		flexDirection: 'column',
		alignItems: 'center',
		// justifyContent: 'center',
	},

	header: {
		fontWeight: 'bold',
		color: '#ddd',
		textShadowColor: '#222',
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 5,
		textAlign: 'center',
		marginTop: 20,
		fontSize: 36,
	},

	teamsContainer: {
		borderWidth: 1,
		borderColor: '#015f2c',
		backgroundColor: '#001b05',
		margin: 10,
		padding: 5,
		borderRadius: 10,
	},

	addTeamContainer: {
		marginTop: 50,
		// borderWidth: 1,
		borderRadius: 25,
		height: 100,
		width: 275,
		flexDirection: 'column',
		backgroundColor: '#ddd',
		// borderRadius: 20,
		// alignSelf: 'stretch',
		overflow: 'hidden',
		borderWidth: 5,
		borderColor: '#08310f',
		justifyContent: 'center',
	},

		addNew: {
		color: '#01e40c',
		textAlign: 'center',
		fontSize: 36,
		textShadowColor: '#333',
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 5,
	},


});
