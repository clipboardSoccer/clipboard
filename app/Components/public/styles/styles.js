'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
let CIRCLE_RADIUS = 36;
let Window = Dimensions.get('window');

module.exports = StyleSheet.create({

//*************************** Welcome ***************************

	welcomeScreen: {
		backgroundColor: '#02c32f',
		height: screenHeight,
	},




//*************************** PageTabs ***************************
	tabContent: {
		borderColor: '#fff',
		borderWidth: 1,
		borderStyle: 'solid',
		flex: 1,
		position: 'relative',
  	},
	tabText: {
		color: 'white',
		marginTop: 20,
		textAlign: 'center',
	},
  loginContainer: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
    padding: 20,
    marginTop: Platform.OS === 'android' ? 56 : 0,
  },
	// container: {
	// 	flex: 1,
	// 	alignItems: 'stretch',
	// 	backgroundColor: '#F5FCFF',
	// 	padding: 20,
	// 	marginTop: Platform.OS === 'android' ? 56 : 0,
	// },

	background: {
		flex: 1,
		resizeMode: 'cover',
		width: screenWidth
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},



//*************************** Sliders ***************************
	slider: {
		flexDirection: 'row',
	},
	scrollView: {
		flex: 1,
	},
	row: {
		flexDirection: 'row',
	},
	col: {
		// flex: 1,
		flexDirection: 'column',
		marginLeft: 7, marginRight: 7,
	},
	legendLabel: {
		textAlign: 'left',
		color: '#fff',
		// marginTop: 10, marginBottom: 20,
		fontSize: 12,
		fontWeight: '300',
	},
	border: {
		borderWidth: 1,
		borderStyle: 'solid'
	},
	button: {
		color: '#ccc',
		alignItems: 'center'
	},
//*************************** Buttons ***************************
	buttonContainer: {
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center'
	},
	buttons: {
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'black',
		width: 100,
		margin: 10
	},
//*************************** Players ***************************
	noPlayerContainer: {
		flex: 1,
		height: 180,
		// alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center',
		// borderWidth: 1,
		// borderStyle: 'solid',
		// borderColor: 'black',
	},
	noPlayer: {
		// width: 200,
		textAlign: 'center',
		height: 50,
		marginTop: 50,
	},
	players: {
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'black',
		// height: 200,
		// alignItems: 'center',
		height: 200,
		width: screenWidth,
		flexDirection: 'row',
		backgroundColor: '#333',
		position: 'relative',
		// justifyContent: 'center'

	},
	// container: {
	// 	height: 200,
	// 	alignItems: 'center',
	// 	flexDirection: 'row',
	// 	justifyContent: 'center',
	// 	borderWidth: 1,
	// 	borderStyle: 'solid',
	// 	borderColor: 'orange',
	// 	backgroundColor: '#777',
	// 	position: 'relative',
	// },
	playerContainer: {
		flexDirection: 'column',
		flexWrap: 'nowrap',
		alignItems: 'center',
		// borderWidth: 1,
		// borderStyle: 'solid',
		// borderColor: 'red',
		padding: 5,
		// paddingTop: 10,
		margin: 5,
		width: screenWidth / 2.5,
		// height: 150,
		position: 'relative'
	},
	player: {
		textAlign: 'center',
		flexDirection: 'column',
		fontSize: 20,
		padding: 2,
		// borderWidth: 1,
		// borderStyle: 'solid',
		// borderColor: 'green',
		// width: screenWidth / 2.5
	},
	playerName: {
		fontSize: 12,
		textAlign:'center',
		width: screenWidth / 2.5,
		flexDirection: 'column',
		// borderWidth: 1,
		// borderStyle: 'solid',
		// borderColor: 'black',
		alignItems: 'center',
		// overflow: 'scroll',
	},
	playerNumber: {
		textAlign: 'center',
		fontSize: 64,
		flexDirection: 'column',
		// borderWidth: 1,
		// borderStyle: 'solid',
		// borderColor: 'black',
		alignItems: 'center',
		// position: 'absolute',
		// bottom: 0,
		// left: screenWidth / 7,
		// padding: 0
		// clear: 'both'

	},
	swiper: {
		// fontSize: 24,
		// letterSpacing: 25,
		// textAlign: 'center',
		// bottom: 0,
		// height: 200,
		// borderWidth: 1,
		// borderStyle: 'solid',
		// borderColor: 'blue',
		// backgroundColor: '#333',
		// padding: 10,
	},
	enterGame: {
		position: 'absolute',
		backgroundColor: '#aaa',
		bottom:-50,
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'red',
		// marginTop: 200,
		height: 50,
		width: screenWidth,
		fontSize: 36,
		paddingTop: 3,
	},

// =================== gameview =======================

	gvPlayerContainer: {
		flexDirection: 'column',
		alignItems: 'stretch',
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'black',
		width: screenWidth / 3,
		padding: 5,
		margin: 15
	},

	gvPlayer: {
		textAlign: 'left',
			flexDirection: 'column'
	},

	GameViewButton:{
		flexDirection:'row',
		flexWrap: 'wrap',
		alignItems: 'center',
		justifyContent: 'center',
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'black',
		width: screenWidth / 4,
		padding: 5,

		// margin: 15
	},

	mainContainer: {
		flex: 1
	},

	dropZone: {
		height: 100,
		width: 100,
		flexDirection: 'row',
		backgroundColor:'#2c3e50',
		borderColor: 'red',
		borderStyle: 'solid',
		borderWidth: 1,
	},

	text: {
		marginTop: 25,
		marginLeft: 5,
		marginRight: 5,
		textAlign: 'center',
		color: '#fff'
	},

	draggableContainer: {
		position: 'absolute',
		top: Window.height/2 - CIRCLE_RADIUS,
		left: Window.width/2 - CIRCLE_RADIUS,
	},

	circle: {
		backgroundColor: '#1abc9c',
		width: CIRCLE_RADIUS*2,
		height: CIRCLE_RADIUS*2,
		borderRadius: CIRCLE_RADIUS
	},
});
