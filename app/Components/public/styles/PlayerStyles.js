'use strict';

import React, { Component } from 'react';

import {
	StyleSheet,
	Dimensions,
	Platform
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
// let Window = Dimensions.get('window');

module.exports = StyleSheet.create({
	playerViewWrapperContainer: {
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: 'center',
		borderWidth: 1,
		borderStyle: 'solid',
		borderTopColor: '#96d6c5',
		borderLeftColor: 'orange',
		borderRightColor: 'orange',
		borderBottomColor: 'orange',
		backgroundColor: '#777',
		position: 'relative',
		borderBottomLeftRadius: 20,
		borderBottomRightRadius: 20,
		width: screenWidth-1,
	},

	playerContainer: {
		flexDirection: 'column',
		flexWrap: 'nowrap',
		alignItems: 'center',
		justifyContent: 'center',
		padding: 5,
		margin: 5,
		width: screenWidth / 2.5,
		borderWidth: 2,
		borderColor: 'orange',
		bottom: 0,
		backgroundColor: '#ccc',
		borderRadius: 10,
	},

	player: {
		textAlign: 'center',
		flexDirection: 'column',
		fontSize: 18,
		padding: 2,
	},

	playerName: {
		fontSize: 12,
		textAlign:'center',
		flexDirection: 'column',
		alignItems: 'center',
		color: '#222',
		fontWeight: '700',
		fontFamily: 'Helvetica',
	},

	playerNumber: {
		textAlign: 'center',
		fontSize: 64,
		flexDirection: 'column',
		alignItems: 'center',
		color: '#02f551',
		textShadowColor: '#222',
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 2,
	},

	edit: {
		fontWeight: 'bold',
		color: 'orange',
		textShadowColor: '#555',
		textShadowOffset: {width: 2, height: 2},
		textShadowRadius: 2,
		padding: 3,
	},

});
