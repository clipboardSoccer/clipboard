import React, { Component } from 'react';
import {
	View,
} from 'react-native';

import styles from '../../public/styles/styles.js';

import Layout from './Layout';

import CreateTeam from './CreateTeam';
import ChooseTeams from './ChooseTeams';

import db from '../db/db.js';

export default class GetTeams extends Component {
	constructor(props) {
		super(props)
		this.state = {
			teams: this.props.teams
		}
	}
	//check if user has teams in state from passed in props
	componentWillMount(){
		if(this.state.teams.length > 0){
			return (
				<View>
					{this.haveTeam(this.state.teams)}
				</View>
			)
		} else {
			return (
				<View>
					{this.createTeam(this.state.teams)}
				</View>
			)
		}
	}
	//If you have a team navigate to Choose Teams
	haveTeam(teams){
		let eachTeam = []
		teams.forEach(function(team){
			if(team.Team_Name){				
				eachTeam.push(team)
			}			
		}.bind(this));
		this.props.navigator.push({
			name: 'ChooseTeams',
			passProps: eachTeam

		});
	}
	// If no teams navigate to Create Team
	createTeam(teams){
		this.props.navigator.push({
			name: 'CreateTeam',
			passProps: teams
		});
	}

	render() {
		return(
			<View />

		)
	}
}
