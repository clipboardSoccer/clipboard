import React, { Component } from 'react';
import {
	Text,
	TextInput,
	ScrollView,
	View,
	ListView,
	Platform
} from 'react-native';

import SortableListView from 'react-native-sortable-listview';
import cx from 'classnames';
import Button from 'react-native-button';
var Switch = require('react-native-material-switch');
import styles from './public/styles/CheckinStyles';
import db from '../db/db.js';
let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});


export default class CheckIn extends Component {
	constructor(props) {
		super(props)

		this.state = {
			team: this.props.team,
			isHere: false,
		}
	}

	componentWillMount(){
		players = []
		this.state.team.players.forEach(function(content){
			content.isHere = false
			players.push({content})
		});

		this.setState({
			dataSource: ds.cloneWithRows(players)
		});
	}

	back(){
		db.find({}, function (err, docs) {

			this.props.navigator.push({
				name: 'ChooseTeams',
				passProps: docs
			});

		}.bind(this));
	}

	editTeam(team){
		this.props.navigator.push({
			name: 'Team',
			passProps: team
		});
	}

	handelHere(id, rowID, sectionID){
		let thisRow = this.state.dataSource._dataBlob[rowID][sectionID].content
		thisRow.isHere = true

		let data = this.state.dataSource._dataBlob[rowID]
		this.state.team.players[sectionID].isHere = true

		this.setState({
			dataSource: ds.cloneWithRows(data),
		});
	}

	handelNotHere(id, rowID, sectionID){
		let thisRow = this.state.dataSource._dataBlob[rowID][sectionID].content
		thisRow.isHere = false

		let data = this.state.dataSource._dataBlob[rowID]
		this.state.team.players[sectionID].isHere = false

		this.setState({
			dataSource: ds.cloneWithRows(data),
		});

		this.forceUpdate();
	}

	_renderRow(rowData, rowID, sectionID){

		const greenColor = {
			alignItems: 'center',
			justifyContent: 'center',
			flexDirection: 'row',
			margin: 20,
			borderColor: 'blue',
			borderStyle: 'solid',
			borderWidth: 1,
			borderRadius: 15,
			backgroundColor: '#00fd3a'
		};

		const redColor = {
			alignItems: 'center',
			justifyContent: 'space-between',
			flexDirection: 'row',
			margin: 20,
			borderColor: '#04ff3e',
			borderStyle: 'solid',
			borderWidth: 1,
			borderRadius: 15,
			backgroundColor: '#563e2c',
		};

		const test = rowData.content.isHere === true ? greenColor : redColor ;

		return(
			<View
			style={test}
			>
				<View style={styles.playerNameContainer}>
					<Text
					style={styles.playerFirstName}
					>
						{rowData.content.playerFirstName}
					</Text>

					<Text
					style={styles.playerLastName}
					>
						{rowData.content.playerLastName}
					</Text>
				</View>

				<View
				style={styles.playerNumberContainer}
				>
					<Text
					style={styles.playerNumber}
					>
						{rowData.content.playerNumber}
					</Text>
				</View>

				<View style={styles.buttonContainer}>
					<View style={styles.hereContainer}>
						<Button
						style={styles.hereText}
						onPress={()=> this.handelHere(rowData.content._id, rowID, sectionID)}
						>
						HERE
						</Button>
					</View>
					<Button
					style={styles.undo}
					onPress={() => this.handelNotHere(rowData.content._id, rowID, sectionID)}
					>
							undo
					</Button>
				</View>
			</View>
		);
	}

	render() {
		if(Platform.OS === 'android'){
			return (
				<ScrollView
				style={styles.checkinPageContainer}>
					<View
					style={styles.headerButtonContainer}
					>
						<Button
						onPress={ () => this.back() }
						style={styles.back}>
							back
						</Button>
						<Button
						style={styles.editTeam}
						onPress={ () => this.editTeam(this.state.team) }
						>
							edit team
						</Button>
					</View>

						<ListView
						dataSource={this.state.dataSource}
						renderRow={this._renderRow.bind(this)} />
					<View style={styles.aBuffer}>
					</View>
				</ScrollView>
			)
		}//end of if
		else if(Platform.OS === 'ios'){
			return (
					<ScrollView
					style={styles.checkinPageContainer}
					>
						<View
						style={styles.headerButtonContainer}
						>
							<Button
							onPress={ () => this.back() }
							style={styles.back}
							>
								back
							</Button>
							<Button
							style={styles.editTeam}
							onPress={ () => this.editTeam(this.state.team) }
							>
								edit team
							</Button>
						</View>

						<ListView
						dataSource={this.state.dataSource}
						renderRow={this._renderRow.bind(this)} />
					</ScrollView>
				)
			}
		}//end of else if
}
