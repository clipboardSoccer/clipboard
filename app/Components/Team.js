import React, { Component } from 'react';
import {
	Text,
	TextInput,
	View,
	Alert,
} from 'react-native';

import Button from 'react-native-button';
import styles from './public/styles/TeamStyles.js';
import db from '../db/db.js';
import Sliders from './utils/Sliders.js';
import Players from './Players.js';
import PlayerDef from '../../logic/PlayerDef.js'

export default class Team extends Component {
	constructor(props) {
		super(props)
		this.state = {
			team: this.props.team,
			players: '',
			playerFirstName: '',
			playerLastName: '',
			playerNumber: '',
		}
	}

	componentWillMount(){

		if(this.state.team){

			this.setState({
				players: this.state.team.players
			});
		}
	}

	submitPlayer(){
		if(this.state.playerFirstName.length < 1 ) {
			let alertMessage = 'Player first name is needed now and can be edited later.';
			Alert.alert('Player First Name Needed', alertMessage);
			return
		}
		if(this.state.playerLastName.length < 1 ) {
			let alertMessage = 'Player last name is needed now and can be edited later.';
			Alert.alert('Player Last Name Needed', alertMessage);
			return
		}
		if(this.state.playerNumber.length < 1 ) {
			let alertMessage = 'Player number is needed now and can be edited later.';
			Alert.alert('Player Number Needed', alertMessage);
			return
		}

		if(!this.state.id){
			// debugger;
			let newPlayer = {
				teamId: this.state.team.id,
				playerFirstName: this.state.playerFirstName.trim(),
				playerLastName: this.state.playerLastName.trim(),
				playerNumber: this.state.playerNumber.trim(),
				Speed: this.refs.Speed.refs.valueText.state.curValue,
				Shooting: this.refs.Shooting.refs.valueText.state.curValue,
				Control: this.refs.Control.refs.valueText.state.curValue,
				Passing: this.refs.Passing.refs.valueText.state.curValue,
				Size: this.refs.Size.refs.valueText.state.curValue
			}

			db.insert(newPlayer, function (err, cb){
				this.setState({
					players: cb
			});

			}.bind(this));

			this.setState({
				playerFirstName: '',
				playerLastName: '',
				playerNumber: '',

			});

			this.refs.Speed.refs.sliderWithValue.value = 5
			this.refs.Shooting.refs.sliderWithValue.value = 5
			this.refs.Control.refs.sliderWithValue.value = 5
			this.refs.Passing.refs.sliderWithValue.value = 5
			this.refs.Size.refs.sliderWithValue.value = 5
			this.updatePlayers();
		}

		if(this.state.id){
			// debugger;

			db.remove({ _id: this.state.id }, {}, function (err, numRemoved) {});

			let newPlayer = {
				teamId: this.state.team.id,
				playerFirstName: this.state.playerFirstName.trim(),
				playerLastName: this.state.playerLastName.trim(),
				playerNumber: this.state.playerNumber.trim(),
				Speed: this.refs.Speed.refs.valueText.state.curValue,
				Shooting: this.refs.Shooting.refs.valueText.state.curValue,
				Control: this.refs.Control.refs.valueText.state.curValue,
				Passing: this.refs.Passing.refs.valueText.state.curValue,
				Size: this.refs.Size.refs.valueText.state.curValue
			}

			db.insert(newPlayer, function (err, cb){
				this.setState({
					players: cb
			});

			}.bind(this));

			this.setState({
				playerFirstName: '',
				playerLastName: '',
				playerNumber: '',
				id: '',

			});

			this.refs.Speed.refs.sliderWithValue.value = 5
			this.refs.Shooting.refs.sliderWithValue.value = 5
			this.refs.Control.refs.sliderWithValue.value = 5
			this.refs.Passing.refs.sliderWithValue.value = 5
			this.refs.Size.refs.sliderWithValue.value = 5
			this.updatePlayers();
		}
	}

	updatePlayers(){

		db.find({teamId: this.state.team.id}, function (err, docs) {

			this.setState({
				players: docs
			});

		}.bind(this));
	}

	remove(){

		if(!this.state.id){
			let alertMessage = 'Select a player to REMOVE using EDIT below';
			Alert.alert('Remove Player', alertMessage);
			return
		}

		if(this.state.id){
			db.remove({ _id: this.state.id }, {}, function (err, numRemoved) {
				db.find({teamId: this.state.team.id}, function (err, docs) {

					this.setState({
						players: docs,
						playerFirstName: '',
						playerLastName: '',
						playerNumber: '',
						id: '',
					});

				}.bind(this));
			}.bind(this));

		}

	}

	editPlayer(edit){

		this.refs.Control.refs.sliderWithValue.value = parseInt(edit.Control);
		this.refs.Passing.refs.sliderWithValue.value = parseInt(edit.Passing);
		this.refs.Shooting.refs.sliderWithValue.value = parseInt(edit.Shooting);
		this.refs.Size.refs.sliderWithValue.value = parseInt(edit.Size);
		this.refs.Speed.refs.sliderWithValue.value = parseInt(edit.Speed);

		this.setState({
			playerFirstName: edit.playerFirstName,
			playerLastName: edit.playerLastName,
			playerNumber: edit.playerNumber,
			id: edit.id,
		});
	}

	enterGame(){
		let team = this.state.team
		let players = this.state.players
		team.players = players

		this.props.navigator.push({
			name: 'Layout',
			passProps: team
		});
	}

	render(){

		return (
			<View style={styles.pageContainer}>
				<Text style={styles.header}>Add Players</Text>
				<View style={styles.textInputContainer}>
					<TextInput
					style={styles.textInput}
					placeholder='Player first name: (Max 15)'
					maxLength={15}
					clearButtonMode='always'
					autoCorrect={false}
					returnKeyType={'done'}
					onChangeText={(value) => this.setState({playerFirstName: value})}
					value={(this.state.playerFirstName)}
					/>

					<TextInput
					style={styles.textInput}
					placeholder='Player last name: (Max 15)'
					maxLength={15}
					clearButtonMode='always'
					autoCorrect={false}
					returnKeyType={'done'}
					onChangeText={(value) => this.setState({playerLastName: value})}
					value={(this.state.playerLastName)}
					/>

					<TextInput
					style={styles.textInput}
					placeholder='Player number: (Max 2)'
					maxLength={2}
					clearButtonMode='always'
					autoCorrect={false}
					returnKeyType={'done'}
					onChangeText={(value) => this.setState({playerNumber: value})}
					value={(this.state.playerNumber)}
					/>
				</View>
				<View style={styles.sliderContainer}>
					<Sliders ref='Control' type='Control' />
					<Sliders ref='Passing' type='Passing' />
					<Sliders ref='Shooting' type='Shooting' />
					<Sliders ref='Size' type='Size' />
					<Sliders ref='Speed' type='Speed'/>
				</View>
				<View style={styles.buttonContainer}>
					<Button style={styles.submitButton} onPress={this.submitPlayer.bind(this)}>
						Submit
					</Button>
					<Button style={styles.removeButton} onPress={this.remove.bind(this)}>
						Remove
					</Button>
				</View>
				<View style={styles.players}>
					<Players editPlayer={this.editPlayer.bind(this)} players={this.state.players}/>
				</View>
				<View>
					<Button
					style={styles.enterGame}
					onPress={this.enterGame.bind(this)}
					>
						Enter Game
					</Button>
				</View>
			</View>
		)
	}
}
