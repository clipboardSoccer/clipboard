'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions
} from 'react-native';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;

module.exports = StyleSheet.create({

	tabContent: {
    	flex: 1,
    	alignItems: 'center',
  	},
  	tabText: {
    	color: 'white',
    	margin: 50,
  	},
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	background: {
		flex: 1,
		resizeMode: 'cover',
		width: screenWidth
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	test: {
		color: 'red'
	}

});
