![Image](http://www.clipartbest.com/cliparts/Kij/eBL/KijeBLyRT.png)
# Clipboard- The Assistant Coaching App
> Clipboard is a game day savior, designed for the soccer parent turned coach, 
> who couldn’t get any of the other parents to sign up again this year. 
> Getting kids to the game on time, and with a snack is hard enough. 
> The coach is also sending group texts, keeping attendance sheets, 
> and getting players on to the field, all while trying to maximize their young athletic potential. 
> Clipboard aggregates each player’s stats (based on coach in-put) 
> to automatically place the best player in attendance, 
> into the best position on the field per quarter, using traditional coaching strategies. More stats = more goals!

# Built with 
* [React Native](https://facebook.github.io/react-native/)
* [React](https://facebook.github.io/react/)
### Packages used
* [RN Button](https://github.com/ide/react-native-button)
* [RN Local MongoDB](https://github.com/smartdemocracy/react-native-local-mongodb) for AsyncStorage abstraction and local data persistance 
* [RN Material Kit](https://github.com/xinthink/react-native-material-kit) for slider functionality
* [RN Pop-up](https://facebook.github.io/react-native/docs/modal.html) for Modal functionality
* [RN Swiper](https://github.com/leecade/react-native-swiper)
* [RN Timer](https://www.npmjs.com/package/react-native-timer)
* [RN Timer-Mixin](https://www.npmjs.com/package/react-native-timer-mixin)
* [RN Vector Icons](https://github.com/oblador/react-native-vector-icons) 
 

# Getting Started

## Choose A Team
> With Clipboard build teams in advance of the game, and make changes on game day.
> Select your team on the TEAM page, or starting from scratch? Go ahead and...

## Create A Team
> Enter your team’s name, if you need a new team name and are looking for inspiration, we suggest “The Jaguars!” Meow.

[![Screen View](http://share.gifyoutube.com/L9OZwA.gif)](https://www.youtube.com/watch?v=Bh2LbvgbAxA)

---

## Add Players
> Let’s make it personal! Here you enter the player: First Name, Last Name and Jersey Number.

## STATS
> Player statistics are organized by SPEED, SHOOTING, CONTROL, PASSING, and SIZE. Each statistic is on a scale of 0-100. Default value is 5, if you’re in a rush.
> Hit SUBMIT, the player is added to the TEAM and their stats are saved.

[![Screen View](http://share.gifyoutube.com/mZmqgr.gif)](https://www.youtube.com/watch?v=V-I6sAGrvd0)

---

> Player’s stats can be edited here or from the TEAM or GAME pages.

[![Screen View](http://share.gifyoutube.com/W6rP0v.gif)](https://www.youtube.com/watch?v=s-PfqE9AUNg)

---

## Team
> It’s time to do a little dance, because this is where you can easily keep tabs on Player attendance. Tap HERE, the player will turn green and be added to the GAME.
> Whoops did you tap Chris P. instead of Chris E.? That’s okay, just tap UNDO
> Oh wait, there’s Chris P. tap them HERE and they’re back in the game…

[![Screen View](http://share.gifyoutube.com/73m90r.gif)](https://www.youtube.com/watch?v=lqU_nO4P02w)

---

## Game
> Ok, it’s game time!
> Toggle at the bottom of the screen to game. All of the checked-in players have been placed on the field. The top quadrant are the 2 players Forward, 2nd quadrant 3 players for Mid, 3rd quadrant are the 3 players on Defense, the rest in the bottom quadrant are on the Bench.
> Don’t like the set up? That’s okay we’re not perfect. Tap the player you want to swap, with the player you want to move into that position.  

[![Screen View](http://share.gifyoutube.com/XDvr18.gif)](https://www.youtube.com/watch?v=kwaQ3OKLIvg)

> Player stats can be updated IN GAME! If Sam or Chris ran faster today, be sure to give them a point in SPEED!

[![Screen View](http://share.gifyoutube.com/wpzKPg.gif)](https://www.youtube.com/watch?v=1lSRAOfcOno)

> Tap GAME START and the timer for the quarter will begin.
> The quarter timer runs out, the players are rearranged, manual override if needed.
> Tap NEXT QUARTER

[![Screen View](http://share.gifyoutube.com/gJOpPG.gif)](https://www.youtube.com/watch?v=yEhTu19Ec2o)

---
> The more you use Clipboard and tune the stats to your team the smarter the app becomes! 

# Clipboard coming soon to iTunes store and Android Market