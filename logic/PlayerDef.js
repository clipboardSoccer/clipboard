
class PlayerDef {
  constructor(fname, lname, speed, shooting, control, passing, size, keeper){
    const FACTORS = 4;
    this._attendence = false;
    this._fname = fname;
    this._lname = lname;
    this._speed = speed;
    this._shooting = shooting;
    this._control = control;
    this._passing = passing;
    this._size = size;
    this._isKeeper = keeper;
    this._rating = 0;
    this._aggregate  = 0;
    this._1stPosition = null;
    this._2ndPosition = null;
    this._3rdPosition = null;
  }
  getPlayerName(){
    return this._name;
  }
  defPlayerPosition(){
    //this is where the definition logic will be
    console.log(this);
    const keeper = 'G';
    const forward = 'F/W';
    const midfield = 'M';
    const defender = 'D';

    if(this._isKeeper === true){
      this._1stPosition = keeper;
      console.log(this._1stPosition);
      return;
    }
    else{
      let forwardVal = this._speed + this._shooting;
      let midfieldVal = this._control + this._passing;

      if(forwardVal >= midfieldVal){
        this._1stPosition = forward;
        this._2ndPosition = midfield;
        this._3rdPosition = defender;
        console.log(this._1stPosition);
        return;
      }
      else if(midfieldVal > forwardVal){
        this._1stPosition = midfield;
        this._2ndPosition = forward;
        this._3rdPosition = defender;
        console.log(this._1stPosition);
        return;
      }

      //define aggregate score
      this._aggregate = (forwardVal + midfieldVal)/FACTORS
    }
  }
}
//
let foo = new PlayerDef("Christian",84,84,84,84,0,false)
let bar = new PlayerDef("Christian",84,84,84,84,0,false)
